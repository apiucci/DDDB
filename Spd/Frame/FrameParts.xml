<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DDDB SYSTEM "git:/DTD/geometry.dtd">
<DDDB>
<!--
  **************************************************************************
  *                                                                        *
  *  Version 2.0 of the first detailed Spd description by Grigori Rybkine  *
  *                         Grigori.Rybkine@cern.ch                        *
  *                                                                        *
  **************************************************************************
-->

<!-- *************************************************************** -->
<!--                Geometry of the Spd Support Frame                -->
<!--                   Logical Volumes Definition                    -->
<!-- *************************************************************** -->

<!-- ******************************************************************* -->
<!--                  Geometry of the Frame Basic Unit                   -->
<!-- ******************************************************************* -->
  <logvol name = "FrameTwins" material = "Air">
    <box name  = "Frame_Twins_Box"
         sizeX = "2.*SpdModXYSize"
         sizeY = "SpdModXYSize"
         sizeZ = "SpdFrameLength"/>

    <paramphysvol2D number1 = "2" number2 = "2">
      <physvol name = "SideBeamPlates"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/SideBeamXYPlate">
	<posXYZ x = "SpdPlateXStep"
                z = "-0.5*PlateZStep"/>
      </physvol>
      <posXYZ x = "-2.*SpdPlateXStep"/>
      <posXYZ z = "PlateZStep"/>
    </paramphysvol2D>

    <paramphysvol number = "2">
      <physvol name = "CentreBeamPlates"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/CentreBeamXYPlate">
	<posXYZ z = "-0.5*PlateZStep"/>
      </physvol>
      <posXYZ z = "PlateZStep"/>
    </paramphysvol>

    <paramphysvol number = "3">
      <physvol name = "BeamTrunkPlates"
	       logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/BeamTrunkPlate">
	<posXYZ x = "-SpdModXYSize+0.5*Beam_Thick"/>
      </physvol>
      <posXYZ x = "SpdModXYSize-0.5*Beam_Thick"/>
    </paramphysvol>

    <paramphysvol2D number1 = "2" number2 = "2">
      <physvol name = "Rods"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/Rod">
	<posXYZ x = "-0.5*SpdModXYSize"
	        y = "SpdHalfStripY+0.5*HalfStrip_Width-0.5*Rod_Width"
	        z = "SpdRod_Offset"/>
      </physvol>
      <posXYZ x = "SpdModXYSize"/>
      <posXYZ y = "0.5*SpdModXYSize"/>
    </paramphysvol2D>

    <paramphysvol2D number1 = "2" number2 = "2">
      <physvol name = "HalfStrips"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/HalfStrip">
	<posXYZ x = "-0.5*SpdModXYSize"
	        y = "SpdHalfStripY"
	        z = "SpdStrip_Offset"/>
      </physvol>
      <posXYZ x = "SpdModXYSize"/>
      <posXYZ y = "-2*SpdHalfStripY"/>
    </paramphysvol2D>

  </logvol>

<!-- ******************************************************************* -->
<!--             Geometry of the Frame One-And-Half Units                -->
<!-- ******************************************************************* -->

<!-- *************************************************************** -->
<!--                  Frame Left One-And-Half Unit                   -->
<!--                   Logical Volume Definition                     -->
<!-- *************************************************************** -->
  <logvol name = "FrameLeftOneAndHalf" material = "Air">
    <box name  = "Frame_Left_One_And_Half_Box"
         sizeX = "1.5*SpdModXYSize"
         sizeY = "SpdModXYSize"
         sizeZ = "SpdFrameLength"/>

    <paramphysvol number = "2">
      <physvol name = "OneAndHalfLeftBeamPlates"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/SideBeamXYPlate">
	<posXYZ x = "0.75*SpdModXYSize-0.5*BeamC_XSize"
	        z = "-0.5*PlateZStep"/>
      </physvol>
      <posXYZ z = "PlateZStep"/>
    </paramphysvol>

    <paramphysvol number = "2">
      <physvol name = "LeftOneAndHalfCentreBeamPlates"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/CentreBeamXYPlate">
	<posXYZ x = "-0.25*SpdModXYSize"
	        z = "-0.5*PlateZStep"/>
      </physvol>
      <posXYZ z = "PlateZStep"/>
    </paramphysvol>

    <paramphysvol number = "2">
      <physvol name = "LeftOneAndHalfBeamTrunkPlates"
	       logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/BeamTrunkPlate">
	<posXYZ x = "-0.25*SpdModXYSize"/>
      </physvol>
      <posXYZ x = "SpdModXYSize-0.5*Beam_Thick"/>
    </paramphysvol>

    <paramphysvol number = "2">
      <physvol name = "LeftRods"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/Rod">
	<posXYZ x = "0.25*SpdModXYSize"
	        y = "SpdHalfStripY+0.5*HalfStrip_Width-0.5*Rod_Width"
	        z = "SpdRod_Offset"/>
      </physvol>
      <posXYZ y = "0.5*SpdModXYSize"/>
    </paramphysvol>

    <paramphysvol number = "2">
      <physvol name = "LeftHalfStrips"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/HalfStrip">
	<posXYZ x = "0.25*SpdModXYSize"
	        y = "SpdHalfStripY"
	        z = "SpdStrip_Offset"/>
      </physvol>
      <posXYZ y = "-2*SpdHalfStripY"/>
    </paramphysvol>

    <paramphysvol number = "2">
      <physvol name = "LeftHalfRods"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/HalfRod">
	<posXYZ x = "-0.75*SpdModXYSize+0.25*SpdRod_Length"
	        y = "SpdHalfStripY+0.5*HalfStrip_Width-0.5*Rod_Width"
	        z = "SpdRod_Offset"/>
      </physvol>
      <posXYZ y = "0.5*SpdModXYSize"/>
    </paramphysvol>

    <paramphysvol number = "2">
      <physvol name = "LeftQuarterStrips"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/QuarterStrip">
	<posXYZ x = "-0.75*SpdModXYSize+0.25*SpdStrip_Length"
	        y = "SpdHalfStripY"
	        z = "SpdStrip_Offset"/>
      </physvol>
      <posXYZ y = "-2*SpdHalfStripY"/>
    </paramphysvol>

  </logvol>

<!-- ******************************************************************* -->
<!--                Geometry of the Frame Beams Parts                    -->
<!--                   Logical Volumes Definition                        -->
<!-- ******************************************************************* -->
  <logvol name = "SideBeamXYPlate" material = "Aluminium" >
    <box name  = "SideBeamXYPlate_Box"
         sizeX = "BeamC_XSize"
         sizeY = "SpdModXYSize"
         sizeZ = "Beam_Thick"/>
  </logvol>

  <logvol name = "CentreBeamXYPlate" material = "Aluminium" >
    <box name  = "CentreBeamXYPlate_Box"
         sizeX = "BeamI_XSize"
         sizeY = "SpdModXYSize"
         sizeZ = "Beam_Thick"/>
  </logvol>

  <logvol name = "BeamTrunkPlate" material = "Aluminium" >
    <box name  = "BeamTrunkPlate_Box"
         sizeX = "Beam_Thick"
         sizeY = "SpdModXYSize"
         sizeZ = "BeamC_ZSize-2.*Beam_Thick"/>
  </logvol>

<!-- ******************************************************************* -->
<!--                   Geometry of the Frame Rods                        -->
<!--                   Logical Volumes Definition                        -->
<!-- ******************************************************************* -->
  <logvol name = "Rod" material = "Aluminium" >
    <box name  = "Rod_Box"
         sizeX = "SpdRod_Length"
         sizeY = "Rod_Width"
         sizeZ = "Rod_Thick"/>
  </logvol>

  <logvol name = "HalfRod" material = "Aluminium" >
    <box name  = "HalfRod_Box"
         sizeX = "0.5*SpdRod_Length"
         sizeY = "Rod_Width"
         sizeZ = "Rod_Thick"/>
  </logvol>

<!-- ******************************************************************* -->
<!--                  Geometry of the Frame Strips                       -->
<!--                   Logical Volumes Definition                        -->
<!-- ******************************************************************* -->
  <logvol name = "HalfStrip" material = "Spd/SpdDrilledAluminium" >
    <box name  = "HalfStrip_Box"
         sizeX = "SpdStrip_Length"
         sizeY = "HalfStrip_Width"
         sizeZ = "Strip_Thick"/>
  </logvol>

  <logvol name = "QuarterStrip" material = "Spd/SpdDrilledAluminium" >
    <box name  = "QuarterStrip_Box"
         sizeX = "0.5*SpdStrip_Length"
         sizeY = "HalfStrip_Width"
         sizeZ = "Strip_Thick"/>
  </logvol>

<!-- *************************************************************** -->
<!--                 Frame Right One-And-Half Unit                   -->
<!--                   Logical Volume Definition                     -->
<!-- *************************************************************** -->
  <logvol name = "FrameRightOneAndHalf" material = "Air">
    <box name  = "Frame_Right_One_And_Half_Box"
         sizeX = "1.5*SpdModXYSize"
         sizeY = "SpdModXYSize"
         sizeZ = "SpdFrameLength"/>

    <paramphysvol number = "2">
      <physvol name = "OneAndHalfRightBeamPlates"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/SideBeamXYPlate">
	<posXYZ x = "-0.75*SpdModXYSize+0.5*BeamC_XSize"
	        z = "-0.5*PlateZStep"/>
      </physvol>
      <posXYZ z = "PlateZStep"/>
    </paramphysvol>

    <paramphysvol number = "2">
      <physvol name = "RightOneAndHalfCentreBeamPlates"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/CentreBeamXYPlate">
	<posXYZ x = " 0.25*SpdModXYSize"
	        z = "-0.5*PlateZStep"/>
      </physvol>
      <posXYZ z = "PlateZStep"/>
    </paramphysvol>

    <paramphysvol number = "2">
      <physvol name = "RightOneAndHalfBeamTrunkPlates"
	       logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/BeamTrunkPlate">
	<posXYZ x = "0.25*SpdModXYSize"/>
      </physvol>
      <posXYZ x = "-SpdModXYSize+0.5*Beam_Thick"/>
    </paramphysvol>

    <paramphysvol number = "2">
      <physvol name = "RightRods"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/Rod">
	<posXYZ x = "-0.25*SpdModXYSize"
	        y = "SpdHalfStripY+0.5*HalfStrip_Width-0.5*Rod_Width"
	        z = "SpdRod_Offset"/>
      </physvol>
      <posXYZ y = "0.5*SpdModXYSize"/>
    </paramphysvol>

    <paramphysvol number = "2">
      <physvol name = "RightHalfStrips"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/HalfStrip">
	<posXYZ x = "-0.25*SpdModXYSize"
	        y = "SpdHalfStripY"
	        z = "SpdStrip_Offset"/>
      </physvol>
      <posXYZ y = "-2*SpdHalfStripY"/>
    </paramphysvol>

    <paramphysvol number = "2">
      <physvol name = "RightHalfRods"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/HalfRod">
	<posXYZ x = "+0.75*SpdModXYSize-0.25*SpdRod_Length"
	        y = "SpdHalfStripY+0.5*HalfStrip_Width-0.5*Rod_Width"
	        z = "SpdRod_Offset"/>
      </physvol>
      <posXYZ y = "0.5*SpdModXYSize"/>
    </paramphysvol>

    <paramphysvol number = "2">
      <physvol name = "RightQuarterStrips"
               logvol = "/dd/Geometry/DownstreamRegion/Spd/Frame/QuarterStrip">
	<posXYZ x = "+0.75*SpdModXYSize-0.25*SpdStrip_Length"
	        y = "SpdHalfStripY"
	        z = "SpdStrip_Offset"/>
      </physvol>
      <posXYZ y = "-2*SpdHalfStripY"/>
    </paramphysvol>

  </logvol>

<!-- ******************************************************************* -->

<!-- ******************************************************************* -->
</DDDB>

<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DDDB SYSTEM "git:/DTD/structure.dtd">
<DDDB>

  <!-- ********************************************** -->
  <!-- *  Define detector element for Hcal detector * -->
  <!-- ********************************************** -->

  <parameter name="InCell"   value="131.3*mm"  />
  <parameter name="OutCell"  value="2.0*InCell"  />
  <parameter name="OutID"       value="0"  />
  <parameter name="InID"        value="1"  />
  <parameter name="ASide"       value="1"  />
  <parameter name="CSide"       value="-1"  />




  <detelem   name    = "Hcal"
             classID = "8900"  >

    <author>  Olivier Callot </author>
    <version> 2.0            </version>

    <geometryinfo lvname  = "/dd/Geometry/DownstreamRegion/Hcal/Installation/lvHcal"
                  condition = "/dd/Conditions/Alignment/Hcal/HcalSystem"
                  support = "/dd/Structure/LHCb/DownstreamRegion"
                  npath   = "HcalSubsystem" />

    <conditioninfo name      = "Hardware"
                   condition = "/dd/Conditions/ReadoutConf/Hcal/Hardware" />

    <conditioninfo name      = "Readout"
                   condition = "/dd/Conditions/ReadoutConf/Hcal/Readout" />

    <conditioninfo name      = "Monitoring"
                   condition = "/dd/Conditions/ReadoutConf/Hcal/Monitoring" />

    <conditioninfo name      = "Gain"
                   condition = "/dd/Conditions/Calibration/Hcal/Gain" />

    <conditioninfo name      = "Reco"
                   condition = "/dd/Conditions/Calibration/Hcal/Reco" />

    <conditioninfo name      = "Calibration"
                   condition = "/dd/Conditions/Calibration/Hcal/Calibration" />

    <conditioninfo name      = "L0Calibration"
                          condition = "/dd/Conditions/Calibration/Hcal/L0Calibration" />

    <conditioninfo name      = "Quality"
                   condition = "/dd/Conditions/Calibration/Hcal/Quality" />

    <conditioninfo name      = "LEDReference"
                   condition = "/dd/Conditions/Calibration/Hcal/LEDReference" />


    <detelemref href = "#HcalC" />
    <detelemref href = "#HcalA" />

    <userParameter name = "centralHoleX" type="int">        2    </userParameter>
    <userParameter name = "centralHoleY" type="int">        2    </userParameter>
    <userParameter name = "YToXSizeRatio" type = "double" > 1.0  </userParameter>
    <userParameter name = "CodingBit" type = "int">         5    </userParameter>
    <userParameter name = "AdcMax" type = "int">            4095 </userParameter>

<!-- definition of the active Z-acceptance in the local frame -->
    <userParameter name="ZOffset" type="double">
      -0.5*HcalTotSpace+HcalFwFaceLength+3*HcalMsPlStepLength
    </userParameter>
    <userParameter name="ZSize"   type="double"> 6*HcalMsPlStepLength </userParameter>

  </detelem>

<!-- ************************************************************************************** -->

  <detelem classID="8901" name="HcalA">
    <geometryinfo lvname = "/dd/Geometry/DownstreamRegion/Hcal/Installation/lvHcalLeft"
                  condition = "/dd/Conditions/Alignment/Hcal/HcalASystem"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Hcal"
                  npath = "HcalA" />
    <userParameter name="CaloSide" type="int"> ASide </userParameter>
    <detelemref href = "#HcalAOuter" />
    <detelemref href = "#HcalAInner" />
  </detelem>

  <detelem classID="8901" name="HcalC">
    <geometryinfo lvname = "/dd/Geometry/DownstreamRegion/Hcal/Installation/lvHcalRight"
                  condition = "/dd/Conditions/Alignment/Hcal/HcalCSystem"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Hcal"
                  npath = "HcalC" />
    <userParameter name="CaloSide" type="int"> CSide </userParameter>
    <detelemref href = "#HcalCOuter" />
    <detelemref href = "#HcalCInner" />
  </detelem>

<!-- ************************************************************************************** -->

  <detelem classID="8902" name="HcalAInner">
    <geometryinfo lvname = "/dd/Geometry/DownstreamRegion/Hcal/Installation/lvHcalInnerSectionLeft"
	          support = "/dd/Structure/LHCb/DownstreamRegion/Hcal/HcalA"
                  npath = "HcalAInner" />

    <userParameter name="CellSize"> InCell </userParameter>
    <userParameter name="Area"     type="int"> InID   </userParameter>
    <userParameter name="XSize" type="double"> 0.25*HcalTotXSize  </userParameter>
    <userParameter name="YSize" type="double"> 14.0*HcalModYSize+14*HcalYGap </userParameter>
  </detelem>

  <detelem classID="8902" name="HcalAOuter">
    <geometryinfo lvname = "/dd/Geometry/DownstreamRegion/Hcal/Installation/lvHcalOuterSectionLeft"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Hcal/HcalA"
                  npath = "HcalAOuter" />

    <userParameter name="CellSize"> OutCell </userParameter>
    <userParameter name="Area"     type="int"> OutID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*HcalTotXSize  </userParameter>
    <userParameter name="YSize" type="double"> HcalTotYSize </userParameter>
  </detelem>

  <detelem classID="8902" name="HcalCInner">
    <geometryinfo lvname = "/dd/Geometry/DownstreamRegion/Hcal/Installation/lvHcalInnerSectionRight"
	          support = "/dd/Structure/LHCb/DownstreamRegion/Hcal/HcalC"
                  npath = "HcalCInner" />

    <userParameter name="CellSize"> InCell </userParameter>
    <userParameter name="Area"     type="int"> InID   </userParameter>
    <userParameter name="XSize" type="double"> 0.25*HcalTotXSize  </userParameter>
    <userParameter name="YSize" type="double"> 14.0*HcalModYSize+14*HcalYGap </userParameter>
  </detelem>

  <detelem classID="8902" name="HcalCOuter">
    <geometryinfo lvname = "/dd/Geometry/DownstreamRegion/Hcal/Installation/lvHcalOuterSectionRight"
                  support = "/dd/Structure/LHCb/DownstreamRegion/Hcal/HcalC"
                  npath = "HcalCOuter" />
    <userParameter name="CellSize"> OutCell </userParameter>
    <userParameter name="Area"     type="int"> OutID   </userParameter>
    <userParameter name="XSize" type="double"> 0.5*HcalTotXSize  </userParameter>
    <userParameter name="YSize" type="double"> HcalTotYSize </userParameter>
  </detelem>


</DDDB>

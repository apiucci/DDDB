<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- This file has the geometry parameters for Rich1Mirror1 which are the spherical mirrors
     which within the acceptance of LHCb  and are near the beampipe  -->
<!--  Begin   Rich1Mirror1GeomParameters     -->
<!--  This is for the Vertical Rich1.            -->
<!-- Mirror1 from CMA:  There 4 quadrants with 1 mirrorsegment in each quadrant
       The mirrors are positioned using their Center of curvature and tilt angle.
       There is a gap between Mirror1 mirrors in adjacent quadrants. These gaps are
       implemented along phi and theta.
       The Mirror1 copy numbers for the G4 physical volume are as follows. Looking
       downstream from the interaction point along positive Z axis:


        Q0      0               Q1      1


                      beam


        Q3     3                Q2      2



      The Mirror1 has various  spherical segments.
      There is  a Rich1Mirror1Master which contains all the volumes associated to the Mirror1. This is
      made of c4f10 and is a rectangular box.
      There is a rectangular frame named Rich1Mirror1Frame which goes around all the four segments and it is
      made of Aluminium.
      There are four spherical volumes named Rich1Mirror1QuadrantModule which contain the components of the mirror1 in
      each quadrant and it is made of c4f10. In each   Rich1Mirror1QuadrantModule there are three spherical
      segments, one for the glasscoating, second for the innerCarbonFibre segment and the third for the
      outerCarbonfibre segment. Each of the Rich1Mirror1QuadrantModule also contains a set of reinforement cylinders which
      join the innerCarbonFibreSegment and the outerCarbonfibreSegment.

      The hole for the beampipe is cut away using boolean subtraction from all the relevent modules. ie. from the  Rich1Mirror1Master,
      Rich1Mirror1QuadrantModule and the spherical segments inside the Rich1Mirror1QuadrantModule.

      Since we are using the survey measurements of the real mirrors and since each mirror segment is slightly different
      than the other segments, all volumes are unique in the sense that there is a single phyical volume for each logical
      volume for all the components of Mirror1. The copy numbering scheme indicated above is still used when placing the
      volumes.

      The convention of copy numbers here is different than that in the survey measurements. The survey uses the convention 2 1
                                                                                                                            beam
                                                                                                                            4 3
      from EDMS document 860274 as one looks downstream from the interaction point.

      SE Jan-2008.

      Latest modification from the survey measurements of Dave Websdale in August11-2008-SE
 -->

<!--  Begin of Rich1 Mirror1 parameter inputs -->

<parameter name="Rh1Mirror1InnerR" value="2710.0*mm"/>
<parameter name="Rh1Mirror1CaFiThickness" value="1.5*mm"/>
<!--
 The following modified in August 2008 after the latest survey by  DW.
 Further modifications in Dec 2009 after more survey info from DW.
 values until Aug 2008
<parameter name="Rh1Mirror1CCLHCbXTop" value="2.3*mm"/>
<parameter name="Rh1Mirror1CCLHCbYTop" value="839.0*mm"/>
<parameter name="Rh1Mirror1CCLHCbZTop" value="-670.5*mm"/>
<parameter name="Rh1Mirror1CCLHCbXBot" value="2.0*mm"/>
<parameter name="Rh1Mirror1CCLHCbYBot" value="-840.8*mm"/>
<parameter name="Rh1Mirror1CCLHCbZBot" value="-669.0*mm"/>
<parameter name="Rh1Mirror1CCLHCbX" value="2.3*mm"/>
<parameter name="Rh1Mirror1CCLHCbY" value="839.0*mm"/>
<parameter name="Rh1Mirror1CCLHCbZ" value="-670.5*mm"/>
 values until Dec 2009
<parameter name="Rh1Mirror1CCLHCbXTop" value="2.5*mm"/>
<parameter name="Rh1Mirror1CCLHCbYTop" value="839.7*mm"/>
<parameter name="Rh1Mirror1CCLHCbZTop" value="-673.2*mm"/>
<parameter name="Rh1Mirror1CCLHCbXBot" value="1.9*mm"/>
<parameter name="Rh1Mirror1CCLHCbYBot" value="-840.1*mm"/>
<parameter name="Rh1Mirror1CCLHCbZBot" value="-672.1*mm"/>

<parameter name="Rh1Mirror1CCLHCbX" value="2.5*mm"/>
<parameter name="Rh1Mirror1CCLHCbY" value="839.7*mm"/>
<parameter name="Rh1Mirror1CCLHCbZ" value="-673.2*mm"/>

 Now for values used from Dec-10-2009 from DW
-->

<parameter name="Rh1Mirror1CCLHCbXTop" value="1.9*mm"/>       <!-- wrt the LHCborigin -->
<parameter name="Rh1Mirror1CCLHCbYTop" value="841.2*mm"/>
<parameter name="Rh1Mirror1CCLHCbZTop" value="-672.8*mm"/>
<parameter name="Rh1Mirror1CCLHCbXBot" value="1.5*mm"/>       <!-- wrt the LHCborigin -->
<parameter name="Rh1Mirror1CCLHCbYBot" value="-840.8*mm"/>
<parameter name="Rh1Mirror1CCLHCbZBot" value="-671.8*mm"/>

<parameter name="Rh1Mirror1CCLHCbX" value="1.9*mm"/>       <!-- wrt the LHCborigin the top values is kept for backward compatibility-->
<parameter name="Rh1Mirror1CCLHCbY" value="841.2*mm"/>     <!-- these three lines can be removed in the future -->
<parameter name="Rh1Mirror1CCLHCbZ" value="-672.8*mm"/>


<!-- now for the radial sizes -->

<parameter name="Rh1Mirror1CaFiInnerSegInnerR" value="Rh1Mirror1InnerR"/>
<parameter name="Rh1Mirror1CaFiInnerSegOuterR" value="Rh1Mirror1CaFiInnerSegInnerR+Rh1Mirror1CaFiThickness"/>

<parameter name="Rh1Mirror1CaFiCylinderLength" value= "20.0*mm"/>
<parameter name="Rh1Mirror1CaFiCylinderToSegmentGap" value="5.0*mm"/>
<parameter name="Rh1Mirror1CaFiOuterSegInnerR" value = "Rh1Mirror1CaFiInnerSegOuterR+Rh1Mirror1CaFiCylinderLength+2.0*Rh1Mirror1CaFiCylinderToSegmentGap"/>
<parameter name="Rh1Mirror1CaFiOuterSegOuterR" value = "Rh1Mirror1CaFiOuterSegInnerR+Rh1Mirror1CaFiThickness"/>


<!-- the following is nominally 33 mm  composed of 2 layers 1.5 thickness each and the cylider of 20 mm and the two gaps 5 mm each.-->
<parameter name="Rh1Mirror1NominalThickness" value="Rh1Mirror1CaFiOuterSegOuterR-Rh1Mirror1InnerR"/>

<parameter name="Rh1Mirror1CaFiCylinderThickness" value ="0.5*mm"/>
<parameter name="Rh1Mirror1CaFiCylinderInnerRadius" value  ="25.0*mm"/>
<parameter name="Rh1Mirror1CaFiCylinderOuterRadius" value  ="Rh1Mirror1CaFiCylinderInnerRadius+Rh1Mirror1CaFiCylinderThickness"/>

<!-- Now for the quadrant module radial sizes -->
<parameter name= "Rh1Mirror1QuadrantModuleRadialTolerence" value="1.0*mm"/>

<parameter name="Rh1Mirror1QuadrantModuleInnerRadius" value= "Rh1Mirror1InnerR-Rh1Mirror1QuadrantModuleRadialTolerence"/>
<parameter name="Rh1Mirror1QuadrantModuleOuterRadius" value= "Rh1Mirror1CaFiOuterSegOuterR+Rh1Mirror1QuadrantModuleRadialTolerence"/>
<parameter name="Rh1Mirror1QuadrantModuleRadialThickness" value="Rh1Mirror1QuadrantModuleOuterRadius-Rh1Mirror1QuadrantModuleInnerRadius"/>


<!-- the default radius is same for all four quadrants. but for safety four different number created to adapt
     to  any future changes -->

<parameter name="Rh1Mirror1InnerRQ0" value="Rh1Mirror1InnerR"/>
<parameter name="Rh1Mirror1CaFiInnerSegInnerRQ0" value="Rh1Mirror1InnerRQ0"/>
<parameter name="Rh1Mirror1CaFiInnerSegOuterRQ0" value="Rh1Mirror1CaFiInnerSegInnerRQ0+Rh1Mirror1CaFiThickness"/>
<parameter name="Rh1Mirror1CaFiOuterSegInnerRQ0" value = "Rh1Mirror1CaFiInnerSegOuterRQ0+Rh1Mirror1CaFiCylinderLength+2.0*Rh1Mirror1CaFiCylinderToSegmentGap"/>
<parameter name="Rh1Mirror1CaFiOuterSegOuterRQ0" value = "Rh1Mirror1CaFiOuterSegInnerRQ0+Rh1Mirror1CaFiThickness"/>
<parameter name="Rh1Mirror1QuadrantModuleInnerRadiusQ0" value= "Rh1Mirror1InnerRQ0-Rh1Mirror1QuadrantModuleRadialTolerence"/>
<parameter name="Rh1Mirror1QuadrantModuleOuterRadiusQ0" value= "Rh1Mirror1CaFiOuterSegOuterRQ0+Rh1Mirror1QuadrantModuleRadialTolerence"/>
<parameter name="Rh1Mirror1QuadrantModuleRadialThicknessQ0" value="Rh1Mirror1QuadrantModuleOuterRadiusQ0-Rh1Mirror1QuadrantModuleInnerRadiusQ0"/>

<parameter name="Rh1Mirror1InnerRQ1" value="Rh1Mirror1InnerR"/>
<parameter name="Rh1Mirror1CaFiInnerSegInnerRQ1" value="Rh1Mirror1InnerRQ1"/>
<parameter name="Rh1Mirror1CaFiInnerSegOuterRQ1" value="Rh1Mirror1CaFiInnerSegInnerRQ1+Rh1Mirror1CaFiThickness"/>
<parameter name="Rh1Mirror1CaFiOuterSegInnerRQ1" value = "Rh1Mirror1CaFiInnerSegOuterRQ1+Rh1Mirror1CaFiCylinderLength+2.0*Rh1Mirror1CaFiCylinderToSegmentGap"/>
<parameter name="Rh1Mirror1CaFiOuterSegOuterRQ1" value = "Rh1Mirror1CaFiOuterSegInnerRQ1+Rh1Mirror1CaFiThickness"/>
<parameter name="Rh1Mirror1QuadrantModuleInnerRadiusQ1" value= "Rh1Mirror1InnerRQ1-Rh1Mirror1QuadrantModuleRadialTolerence"/>
<parameter name="Rh1Mirror1QuadrantModuleOuterRadiusQ1" value= "Rh1Mirror1CaFiOuterSegOuterRQ1+Rh1Mirror1QuadrantModuleRadialTolerence"/>
<parameter name="Rh1Mirror1QuadrantModuleRadialThicknessQ1" value="Rh1Mirror1QuadrantModuleOuterRadiusQ1-Rh1Mirror1QuadrantModuleInnerRadiusQ1"/>

<parameter name="Rh1Mirror1InnerRQ2" value="Rh1Mirror1InnerR"/>
<parameter name="Rh1Mirror1CaFiInnerSegInnerRQ2" value="Rh1Mirror1InnerRQ2"/>
<parameter name="Rh1Mirror1CaFiInnerSegOuterRQ2" value="Rh1Mirror1CaFiInnerSegInnerRQ2+Rh1Mirror1CaFiThickness"/>
<parameter name="Rh1Mirror1CaFiOuterSegInnerRQ2" value = "Rh1Mirror1CaFiInnerSegOuterRQ2+Rh1Mirror1CaFiCylinderLength+2.0*Rh1Mirror1CaFiCylinderToSegmentGap"/>
<parameter name="Rh1Mirror1CaFiOuterSegOuterRQ2" value = "Rh1Mirror1CaFiOuterSegInnerRQ2+Rh1Mirror1CaFiThickness"/>
<parameter name="Rh1Mirror1QuadrantModuleInnerRadiusQ2" value= "Rh1Mirror1InnerRQ2-Rh1Mirror1QuadrantModuleRadialTolerence"/>
<parameter name="Rh1Mirror1QuadrantModuleOuterRadiusQ2" value= "Rh1Mirror1CaFiOuterSegOuterRQ2+Rh1Mirror1QuadrantModuleRadialTolerence"/>
<parameter name="Rh1Mirror1QuadrantModuleRadialThicknessQ2" value="Rh1Mirror1QuadrantModuleOuterRadiusQ2-Rh1Mirror1QuadrantModuleInnerRadiusQ2"/>

<parameter name="Rh1Mirror1InnerRQ3" value="Rh1Mirror1InnerR"/>
<parameter name="Rh1Mirror1CaFiInnerSegInnerRQ3" value="Rh1Mirror1InnerRQ3"/>
<parameter name="Rh1Mirror1CaFiInnerSegOuterRQ3" value="Rh1Mirror1CaFiInnerSegInnerRQ3+Rh1Mirror1CaFiThickness"/>
<parameter name="Rh1Mirror1CaFiOuterSegInnerRQ3" value = "Rh1Mirror1CaFiInnerSegOuterRQ3+Rh1Mirror1CaFiCylinderLength+2.0*Rh1Mirror1CaFiCylinderToSegmentGap"/>
<parameter name="Rh1Mirror1CaFiOuterSegOuterRQ3" value = "Rh1Mirror1CaFiOuterSegInnerRQ3+Rh1Mirror1CaFiThickness"/>
<parameter name="Rh1Mirror1QuadrantModuleInnerRadiusQ3" value= "Rh1Mirror1InnerRQ3-Rh1Mirror1QuadrantModuleRadialTolerence"/>
<parameter name="Rh1Mirror1QuadrantModuleOuterRadiusQ3" value= "Rh1Mirror1CaFiOuterSegOuterRQ3+Rh1Mirror1QuadrantModuleRadialTolerence"/>
<parameter name="Rh1Mirror1QuadrantModuleRadialThicknessQ3" value="Rh1Mirror1QuadrantModuleOuterRadiusQ3-Rh1Mirror1QuadrantModuleInnerRadiusQ3"/>

<!--  Now for the mirror tilts -->
<parameter name="Rh1Mirror1TopVertTilt" value="(asin(Rh1Mirror1CCLHCbYTop/Rh1Mirror1InnerR))*rad"/>
<parameter name="Rh1Mirror1TopHorizTilt" value="(asin(Rh1Mirror1CCLHCbXTop/Rh1Mirror1InnerR))*rad"/>
<parameter name="Rh1Mirror1BotVertTilt" value="(asin(Rh1Mirror1CCLHCbYBot/Rh1Mirror1InnerR))*rad"/>
<parameter name="Rh1Mirror1BotHorizTilt" value="(asin(Rh1Mirror1CCLHCbXBot/Rh1Mirror1InnerR))*rad"/>


<!-- Mirror tilts in each quadrant -->
<parameter name="Rh1Mirror1VertTiltQ0" value="(asin(Rh1Mirror1CCLHCbYTop/Rh1Mirror1InnerRQ0))*rad"/>
<parameter name="Rh1Mirror1VertTiltQ1" value="(asin(Rh1Mirror1CCLHCbYTop/Rh1Mirror1InnerRQ1))*rad"/>
<parameter name="Rh1Mirror1HorizTiltQ0" value="(asin(Rh1Mirror1CCLHCbXTop/Rh1Mirror1InnerRQ0))*rad"/>
<parameter name="Rh1Mirror1HorizTiltQ1" value="(asin(Rh1Mirror1CCLHCbXTop/Rh1Mirror1InnerRQ1))*rad"/>
<parameter name="Rh1Mirror1VertTiltQ2" value="(asin(Rh1Mirror1CCLHCbYBot/Rh1Mirror1InnerRQ2))*rad"/>
<parameter name="Rh1Mirror1VertTiltQ3" value="(asin(Rh1Mirror1CCLHCbYBot/Rh1Mirror1InnerRQ3))*rad"/>
<parameter name="Rh1Mirror1HorizTiltQ2" value="(asin(Rh1Mirror1CCLHCbXBot/Rh1Mirror1InnerRQ2))*rad"/>
<parameter name="Rh1Mirror1HorizTiltQ3" value="(asin(Rh1Mirror1CCLHCbXBot/Rh1Mirror1InnerRQ3))*rad"/>

<parameter name="Rh1Mirror1SizeAlongXAxis" value="835.0*mm"/>
<parameter name="Rh1Mirror1SizeAlongYAxis" value="642.0*mm"/>
<parameter name="Rh1NumberofMirror1Segements" value="4"/>
<parameter name="Rh1Mirror1NumRows" value="1"/>
<parameter name="Rh1Mirror1NumColumns" value="1"/>

<parameter name="Rh1Mirror1MasterXSize" value   = "1750.0*mm"/>
<parameter name="Rh1Mirror1MasterYSize" value   = "1350.0*mm"/>
<parameter name="Rh1Mirror1MasterZSize" value   = "350.0*mm"/>
<parameter name="Rh1Mirror1MasterXLocationInLHCb" value = "0.0*mm"/>
<parameter name="Rh1Mirror1MasterYLocationInLHCb" value = "0.0*mm"/>
<parameter name="Rh1Mirror1MasterZLocationInLHCb" value = "1900.0*mm"/>

<parameter name="Rh1Mirror1MasterXLocationInRich1SubMaster" value="Rh1Mirror1MasterXLocationInLHCb-Rich1MasterX-Rh1SubMasterX"/>
<parameter name="Rh1Mirror1MasterYLocationInRich1SubMaster" value="Rh1Mirror1MasterYLocationInLHCb-Rich1MasterY-Rh1SubMasterY"/>
<parameter name="Rh1Mirror1MasterZLocationInRich1SubMaster" value="Rh1Mirror1MasterZLocationInLHCb-Rich1MasterZ-Rh1SubMasterZ"/>

<parameter name="Rh1Mirror1QuadrantModuleLateralTolerence" value="0.5*mm"/>

<!-- Now for the quadrant module sizes . Using the same radius, for now in all four quadrants for the angular sizes -->
<parameter name="Rh1Mirror1QuadrantModuleSizeAlongXAxis" value="Rh1Mirror1SizeAlongXAxis+2*Rh1Mirror1QuadrantModuleLateralTolerence"/>
<parameter name="Rh1Mirror1QuadrantModuleSizeAlongYAxis" value="Rh1Mirror1SizeAlongYAxis+2*Rh1Mirror1QuadrantModuleLateralTolerence"/>
<parameter name="Rh1Mirror1QuadrantModuleDeltaTheta" value= "(2.0*asin(Rh1Mirror1QuadrantModuleSizeAlongXAxis/(2.0*Rh1Mirror1QuadrantModuleInnerRadius)))*rad"/>
<parameter name="Rh1Mirror1QuadrantModuleDeltaPhi" value="(2.0*asin(Rh1Mirror1QuadrantModuleSizeAlongYAxis/(2.0*Rh1Mirror1QuadrantModuleInnerRadius)))*rad"/>

<!-- the quadrant is made with a larger than actual phi angular size and then a boolean subtraction is applied to cut away the
     extra part. This is to get straight edge of the spherical segment along the phi edge. -->
<parameter name="Rh1Mirror1QuadrantModuleDeltaThetaExtended"
				 value = "2.0*asin(sin(0.5*Rh1Mirror1QuadrantModuleDeltaTheta)/cos(0.5*Rh1Mirror1QuadrantModuleDeltaPhi))"/>
<parameter name="Rh1Mirror1QuadrantModuleThetaSegmentStart" value= "(pi/2.0)*rad-(0.5*Rh1Mirror1QuadrantModuleDeltaThetaExtended)"/>
<parameter name="Rh1Mirror1QuadrantModulePhiSegmentStart" value = "-0.5*Rh1Mirror1QuadrantModuleDeltaPhi"/>
<parameter name="Rh1Mirror1LargeSizeValue" value="10000.0*mm"/>
<parameter name="Rh1Mirror1QuadrantBoxSubtractXSize" value="Rh1Mirror1LargeSizeValue"/>
<parameter name="Rh1Mirror1QuadrantBoxSubtractYSize" value="Rh1Mirror1LargeSizeValue"/>
<parameter name="Rh1Mirror1QuadrantBoxSubtractZSize" value="Rh1Mirror1LargeSizeValue"/>
<!-- only the non-zero values from the following 5 lines are used in creating the log vol -->
<parameter name="Rh1Mirror1QuadrantBoxSubtractXLocation" value="0.0*mm"/>

<parameter name="Rh1Mirror1QuadrantBoxSubtractYLocationA" value="0.5*(Rh1Mirror1QuadrantModuleSizeAlongYAxis+Rh1Mirror1QuadrantBoxSubtractYSize)"/>
<parameter name="Rh1Mirror1QuadrantBoxSubtractYLocationB" value="-1.0*Rh1Mirror1QuadrantBoxSubtractYLocationA"/>
<parameter name="Rh1Mirror1QuadrantBoxSubtractZLocationV" value="0.0*mm"/>

<parameter name="Rh1Mirror1QuadrantBoxSubtractZLocationHA" value="0.5*(Rh1Mirror1QuadrantModuleSizeAlongXAxis+Rh1Mirror1QuadrantBoxSubtractZSize)"/>
<parameter name="Rh1Mirror1QuadrantBoxSubtractZLocationHB" value="-1.0*Rh1Mirror1QuadrantBoxSubtractZLocationHA"/>

<!-- now to deal with the gaps between the segments -->
<parameter name="Rh1Mirror1XGapBetQuad" value="3.0*mm"/>
<parameter name="Rh1Mirror1YGapBetQuad" value="3.0*mm"/>

<!-- the following tolerences are to avoid overlap between nearby Quadrant modules due to small gaps.
     A  2.5 mm tolerence is sufficient for the nominal orientation. Increased to 3 mm to enable misalignments of the mirrors.
     For large misalignments of mirrors, one should increase the gap between these quadrant modules to
     avoid overlap between nearby modules. SE Jan 2008.

     <parameter name="Rh1Mirror1GapTolerenceInX" value="2.5*mm"/>
     <parameter name="Rh1Mirror1GapTolerenceInY" value="2.5*mm"/>
-->

<parameter name="Rh1Mirror1GapTolerenceInX" value="3.0*mm"/>
<parameter name="Rh1Mirror1GapTolerenceInY" value="3.0*mm"/>

<parameter name="Rh1Mirror1XHalfGapFromThickness" value="sin(0.5*Rh1Mirror1QuadrantModuleDeltaThetaExtended)*(Rh1Mirror1QuadrantModuleRadialThickness)"/>
<parameter name="Rh1Mirror1YHalfGapFromThickness" value="sin(0.5*Rh1Mirror1QuadrantModuleDeltaPhi)*(Rh1Mirror1QuadrantModuleRadialThickness)"/>

<parameter name="Rh1Mirror1PhiGap" value="(2.0*asin(((0.5*Rh1Mirror1YGapBetQuad)+Rh1Mirror1YHalfGapFromThickness+Rh1Mirror1GapTolerenceInY)/(Rh1Mirror1QuadrantModuleInnerRadius)))*rad"/>

<parameter name="Rh1Mirror1ThetaGap" value="(2.0*asin(((0.5*Rh1Mirror1XGapBetQuad)+ Rh1Mirror1XHalfGapFromThickness+Rh1Mirror1GapTolerenceInX)/(Rh1Mirror1QuadrantModuleInnerRadius)))*rad"/>


<!-- now for the beampipe hole: using survey measurement of the real mirror segment for the beam hole.
     It is considered as a cylindrical part near the mirror and is cut away using boolean subtraction -->
<parameter name="Rh1Mirror1NominalBeamHoleRadialSize" value= "61.0*mm"/>
<parameter name="Rh1Mirror1NominalBeamHoleRadialSizeTolerenceForQuadrant" value= "1.0*mm"/>
<parameter name="Rh1Mirror1NominalBeamHoleRadialSizeTolerence" value= "2.0*mm"/>
<parameter name="Rh1Mirror1BeamHoleRadialSizeForQuadrant" value="Rh1Mirror1NominalBeamHoleRadialSize+Rh1Mirror1NominalBeamHoleRadialSizeTolerenceForQuadrant"/>
<parameter name="Rh1Mirror1BeamHoleRadialSizeForQuadrantComponents" value="Rh1Mirror1NominalBeamHoleRadialSize+Rh1Mirror1NominalBeamHoleRadialSizeTolerence"/>

<parameter name="Rh1Mirror1BeamHoleLength" value= "Rh1Mirror1LargeSizeValue"/>
<parameter name="Rh1Mirror1BeamHoleLocationZ" value="0.0*mm"/>
<parameter name="Rh1Mirror1BeamHoleYRotationAngle" value="(pi/2)*rad"/>

<parameter name="Rh1Mirror1BeamHoleLocationYQ0" value="-0.5*Rh1Mirror1QuadrantModuleSizeAlongYAxis"/>
<parameter name="Rh1Mirror1BeamHoleLocationYQ1" value="-0.5*Rh1Mirror1QuadrantModuleSizeAlongYAxis"/>
<parameter name="Rh1Mirror1BeamHoleLocationYQ2" value="0.5*Rh1Mirror1QuadrantModuleSizeAlongYAxis"/>
<parameter name="Rh1Mirror1BeamHoleLocationYQ3" value="0.5*Rh1Mirror1QuadrantModuleSizeAlongYAxis"/>
<parameter name="Rh1Mirror1BeamHoleLocationZQ0" value="0.5*Rh1Mirror1QuadrantModuleSizeAlongXAxis"/>
<parameter name="Rh1Mirror1BeamHoleLocationZQ1" value="-0.5*Rh1Mirror1QuadrantModuleSizeAlongXAxis"/>
<parameter name="Rh1Mirror1BeamHoleLocationZQ2" value="-0.5*Rh1Mirror1QuadrantModuleSizeAlongXAxis"/>
<parameter name="Rh1Mirror1BeamHoleLocationZQ3" value="0.5*Rh1Mirror1QuadrantModuleSizeAlongXAxis"/>


<!-- now to place the quadrants -->
<parameter name = "Rh1Mirror1QuadrantPhiShiftNominal" value="0.5*(Rh1Mirror1QuadrantModuleDeltaPhi+Rh1Mirror1PhiGap)"/>


<!-- the Rh1Mirror1VertTilt and Rh1Mirror1HorizTilt already has the signs for the corresponding positive and negative rotations -->
<parameter name = "Rh1Mirror1QuadrantZRotQ0" value="Rh1Mirror1QuadrantPhiShiftNominal-Rh1Mirror1VertTiltQ0"/>
<parameter name = "Rh1Mirror1QuadrantZRotQ1" value="Rh1Mirror1QuadrantPhiShiftNominal-Rh1Mirror1VertTiltQ1"/>
<parameter name = "Rh1Mirror1QuadrantZRotQ2" value="-1.0*Rh1Mirror1QuadrantPhiShiftNominal-Rh1Mirror1VertTiltQ2"/>
<parameter name = "Rh1Mirror1QuadrantZRotQ3" value="-1.0*Rh1Mirror1QuadrantPhiShiftNominal-Rh1Mirror1VertTiltQ3"/>


<parameter name =  "Rh1Mirror1QuadrantThetaShiftNominal" value="0.5*(Rh1Mirror1QuadrantModuleDeltaThetaExtended+Rh1Mirror1ThetaGap)"/>

<parameter name =  "Rh1Mirror1QuadrantYRotQ0" value="(-(pi/2)*rad)+Rh1Mirror1QuadrantThetaShiftNominal-Rh1Mirror1HorizTiltQ0"/>
<parameter name =  "Rh1Mirror1QuadrantYRotQ1" value="(-(pi/2)*rad)-Rh1Mirror1QuadrantThetaShiftNominal-Rh1Mirror1HorizTiltQ1"/>
<parameter name =  "Rh1Mirror1QuadrantYRotQ2" value="(-(pi/2)*rad)-Rh1Mirror1QuadrantThetaShiftNominal-Rh1Mirror1HorizTiltQ2"/>
<parameter name =  "Rh1Mirror1QuadrantYRotQ3" value="(-(pi/2)*rad)+Rh1Mirror1QuadrantThetaShiftNominal-Rh1Mirror1HorizTiltQ3"/>

<!-- the following estimate for quadrant  X rotation is only a first approximation -->

<!--
     <parameter name = "Rh1Mirror1QuadrantXRotNominal" value= "((pi/2)*(1.0-cos(Rh1Mirror1QuadrantPhiShiftNominal)))*rad"/>
-->
<parameter name = "Rh1Mirror1QuadrantXRotNominalFactor" value= "(1.0-cos(Rh1Mirror1QuadrantPhiShiftNominal))"/>
<parameter name = "Rh1Mirror1QuadrantXRotTolerence" value="-2.0*mrad"/>

<parameter name = "Rh1Mirror1QuadrantXRotQ0" value= "-1.0*(Rh1Mirror1QuadrantXRotNominalFactor*Rh1Mirror1QuadrantYRotQ0+Rh1Mirror1QuadrantXRotTolerence)"/>
<parameter name = "Rh1Mirror1QuadrantXRotQ1" value= "(Rh1Mirror1QuadrantXRotNominalFactor*Rh1Mirror1QuadrantYRotQ1+Rh1Mirror1QuadrantXRotTolerence)"/>
<parameter name = "Rh1Mirror1QuadrantXRotQ2" value="-1.0*(Rh1Mirror1QuadrantXRotNominalFactor*Rh1Mirror1QuadrantYRotQ2 +Rh1Mirror1QuadrantXRotTolerence )"/>
<parameter name = "Rh1Mirror1QuadrantXRotQ3" value= "(Rh1Mirror1QuadrantXRotNominalFactor*Rh1Mirror1QuadrantYRotQ3+Rh1Mirror1QuadrantXRotTolerence )"/>

<parameter name="Rh1Mirror1QuadrantModuleCOCXinQ0" value="Rh1Mirror1CCLHCbXTop-Rich1MasterX-Rh1SubMasterX-Rh1Mirror1MasterXLocationInRich1SubMaster"/>
<parameter name="Rh1Mirror1QuadrantModuleCOCYinQ0" value  ="Rh1Mirror1CCLHCbYTop-Rich1MasterY-Rh1SubMasterY-Rh1Mirror1MasterYLocationInRich1SubMaster"/>
<parameter name="Rh1Mirror1QuadrantModuleCOCZinQ0" value="Rh1Mirror1CCLHCbZTop-Rich1MasterZ-Rh1SubMasterZ-Rh1Mirror1MasterZLocationInRich1SubMaster"/>
<parameter name="Rh1Mirror1QuadrantModuleCOCXinQ1" value="Rh1Mirror1QuadrantModuleCOCXinQ0"/>
<parameter name="Rh1Mirror1QuadrantModuleCOCYinQ1" value="Rh1Mirror1QuadrantModuleCOCYinQ0"/>
<parameter name="Rh1Mirror1QuadrantModuleCOCZinQ1" value  ="Rh1Mirror1QuadrantModuleCOCZinQ0"/>

<parameter name="Rh1Mirror1QuadrantModuleCOCXinQ2" value= "Rh1Mirror1CCLHCbXBot-Rich1MasterX-Rh1SubMasterX-Rh1Mirror1MasterXLocationInRich1SubMaster"/>
<parameter name="Rh1Mirror1QuadrantModuleCOCYinQ2" value= "Rh1Mirror1CCLHCbYBot-Rich1MasterY-Rh1SubMasterY-Rh1Mirror1MasterYLocationInRich1SubMaster"/>
<parameter name="Rh1Mirror1QuadrantModuleCOCZinQ2" value= "Rh1Mirror1CCLHCbZBot-Rich1MasterZ-Rh1SubMasterZ-Rh1Mirror1MasterZLocationInRich1SubMaster"/>
<parameter name="Rh1Mirror1QuadrantModuleCOCXinQ3" value= "Rh1Mirror1QuadrantModuleCOCXinQ2"/>
<parameter name="Rh1Mirror1QuadrantModuleCOCYinQ3" value= "Rh1Mirror1QuadrantModuleCOCYinQ2"/>
<parameter name="Rh1Mirror1QuadrantModuleCOCZinQ3" value= "Rh1Mirror1QuadrantModuleCOCZinQ2"/>


<!-- now the parts inside the quadrant modules . First the two carbonfibre layers -->
<parameter name="Rh1Mirror1CaFiDeltaTheta" value="(2.0*asin(Rh1Mirror1SizeAlongXAxis/(2.0*Rh1Mirror1InnerR)))*rad"/>
<parameter name="Rh1Mirror1CaFiDeltaPhi" value="(2.0*asin(Rh1Mirror1SizeAlongYAxis/(2.0*Rh1Mirror1InnerR)))*rad"/>
<parameter name="Rh1Mirror1CaFiDeltaThetaExtended" value="2.0*asin(sin(0.5*Rh1Mirror1CaFiDeltaTheta)/cos(0.5*Rh1Mirror1CaFiDeltaPhi))"/>
<parameter name="Rh1Mirror1CaFiThetaSegmentStart" value="(pi/2.0)*rad-(0.5*Rh1Mirror1CaFiDeltaThetaExtended)"/>
<parameter name="Rh1Mirror1CaFiPhiSegmentStart" value="-0.5*Rh1Mirror1CaFiDeltaPhi"/>

<parameter name="Rh1Mirror1CaFiBoxSubtractYLocationA" value="0.5*(Rh1Mirror1SizeAlongYAxis+Rh1Mirror1QuadrantBoxSubtractYSize)"/>
<parameter name="Rh1Mirror1CaFiBoxSubtractYLocationB" value="-1.0*Rh1Mirror1CaFiBoxSubtractYLocationA"/>

<parameter name="Rh1Mirror1CaFiBoxSubtractZLocationHA" value="0.5*(Rh1Mirror1SizeAlongXAxis+Rh1Mirror1QuadrantBoxSubtractZSize)"/>
<parameter name="Rh1Mirror1CaFiBoxSubtractZLocationHB" value="-1.0*Rh1Mirror1CaFiBoxSubtractZLocationHA"/>

<parameter name="Rh1Mirror1CaFiInnerSegDeltaTheta" value="Rh1Mirror1CaFiDeltaTheta "/>
<parameter name="Rh1Mirror1CaFiInnerSegDeltaPhi" value="Rh1Mirror1CaFiDeltaPhi"/>
<parameter name="Rh1Mirror1CaFiInnerSegDeltaThetaExtended" value="Rh1Mirror1CaFiDeltaThetaExtended"/>
<parameter name="Rh1Mirror1CaFiInnerSegThetaStart" value="Rh1Mirror1CaFiThetaSegmentStart"/>
<parameter name="Rh1Mirror1CaFiInnerSegPhiStart" value="Rh1Mirror1CaFiPhiSegmentStart"/>
<parameter name="Rh1Mirror1CaFiInnerSegBoxSubtractYLocationA" value="Rh1Mirror1CaFiBoxSubtractYLocationA"/>
<parameter name="Rh1Mirror1CaFiInnerSegBoxSubtractYLocationB" value="Rh1Mirror1CaFiBoxSubtractYLocationB"/>
<parameter name="Rh1Mirror1CaFiInnerSegBoxSubtractZLocationHA" value="Rh1Mirror1CaFiBoxSubtractZLocationHA"/>
<parameter name="Rh1Mirror1CaFiInnerSegBoxSubtractZLocationHB" value="Rh1Mirror1CaFiBoxSubtractZLocationHB"/>

<parameter name="Rh1Mirror1CaFiDeltaThetaOuter" value="(2.0*asin(Rh1Mirror1SizeAlongXAxis/(2.0*Rh1Mirror1CaFiOuterSegInnerR)))*rad"/>
<parameter name="Rh1Mirror1CaFiDeltaPhiOuter" value="(2.0*asin(Rh1Mirror1SizeAlongYAxis/(2.0*Rh1Mirror1CaFiOuterSegInnerR)))*rad"/>
<parameter name="Rh1Mirror1CaFiDeltaThetaExtendedOuter" value="2.0*asin(sin(0.5*Rh1Mirror1CaFiDeltaThetaOuter)/cos(0.5*Rh1Mirror1CaFiDeltaPhiOuter))"/>
<parameter name="Rh1Mirror1CaFiThetaSegmentStartOuter" value="(pi/2.0)*rad-(0.5*Rh1Mirror1CaFiDeltaThetaExtendedOuter)"/>
<parameter name="Rh1Mirror1CaFiPhiSegmentStartOuter" value="-0.5*Rh1Mirror1CaFiDeltaPhiOuter"/>

<parameter name="Rh1Mirror1CaFiOuterSegDeltaTheta" value="Rh1Mirror1CaFiDeltaThetaOuter"/>
<parameter name="Rh1Mirror1CaFiOuterSegDeltaPhi" value="Rh1Mirror1CaFiDeltaPhiOuter"/>
<parameter name="Rh1Mirror1CaFiOuterSegDeltaThetaExtended" value="Rh1Mirror1CaFiDeltaThetaExtendedOuter"/>
<parameter name="Rh1Mirror1CaFiOuterSegThetaStart" value="Rh1Mirror1CaFiThetaSegmentStartOuter"/>
<parameter name="Rh1Mirror1CaFiOuterSegPhiStart" value="Rh1Mirror1CaFiPhiSegmentStartOuter"/>

<parameter name="Rh1Mirror1CaFiOuterSegBoxSubtractYLocationA" value="Rh1Mirror1CaFiBoxSubtractYLocationA"/>
<parameter name="Rh1Mirror1CaFiOuterSegBoxSubtractYLocationB" value="Rh1Mirror1CaFiBoxSubtractYLocationB"/>
<parameter name="Rh1Mirror1CaFiOuterSegBoxSubtractZLocationHA" value="Rh1Mirror1CaFiBoxSubtractZLocationHA"/>
<parameter name="Rh1Mirror1CaFiOuterSegBoxSubtractZLocationHB" value="Rh1Mirror1CaFiBoxSubtractZLocationHB"/>


<!-- Now for placing the cylinders between the layers. a Set of 11 by 8  (11 along X and 8 along Y)  are placed
     with an approximate pitch of 60 mm in each quadrant along the inner segment layer  . It is asymmetric due to the beampipe
     hence the  Rh1Mirror1NumCylAlongX is set as 10 and  Rh1Mirror1NumCylAlongY is set as 7. -->

<parameter name = "Rh1Mirror1NumCylAlongX" value="10"/>
<parameter name = "Rh1Mirror1NumCylAlongY" value="7"/>
<parameter name = "Rh1Mirror1CylPitch" value="60*mm"/>
<parameter name = "Rh1Mirror1CylRadialLocationQ0" value="Rh1Mirror1CaFiInnerSegOuterRQ0+Rh1Mirror1CaFiCylinderToSegmentGap+0.5*Rh1Mirror1CaFiCylinderLength"/>
<parameter name = "Rh1Mirror1CylRadialLocationQ1" value="Rh1Mirror1CaFiInnerSegOuterRQ0+Rh1Mirror1CaFiCylinderToSegmentGap+0.5*Rh1Mirror1CaFiCylinderLength"/>
<parameter name = "Rh1Mirror1CylRadialLocationQ2" value="Rh1Mirror1CaFiInnerSegOuterRQ0+Rh1Mirror1CaFiCylinderToSegmentGap+0.5*Rh1Mirror1CaFiCylinderLength"/>
<parameter name = "Rh1Mirror1CylRadialLocationQ3" value="Rh1Mirror1CaFiInnerSegOuterRQ0+Rh1Mirror1CaFiCylinderToSegmentGap+0.5*Rh1Mirror1CaFiCylinderLength"/>
<parameter name =  "Rh1Mirror1CylThetaStartQ0" value="((pi/2)*rad)-(0.5*Rh1Mirror1NumCylAlongX*Rh1Mirror1CylPitch)/Rh1Mirror1CylRadialLocationQ0"/>
<parameter name =  "Rh1Mirror1CylThetaStartQ1" value="((pi/2)*rad)-(0.5*Rh1Mirror1NumCylAlongX*Rh1Mirror1CylPitch)/Rh1Mirror1CylRadialLocationQ1"/>
<parameter name =  "Rh1Mirror1CylThetaStartQ2" value="((pi/2)*rad)-(0.5*Rh1Mirror1NumCylAlongX*Rh1Mirror1CylPitch)/Rh1Mirror1CylRadialLocationQ2"/>
<parameter name =  "Rh1Mirror1CylThetaStartQ3" value="((pi/2)*rad)-(0.5*Rh1Mirror1NumCylAlongX*Rh1Mirror1CylPitch)/Rh1Mirror1CylRadialLocationQ3"/>
<parameter name =  "Rh1Mirror1CylPhiStartQ0" value="((-0.5*Rh1Mirror1NumCylAlongY*Rh1Mirror1CylPitch)/Rh1Mirror1CylRadialLocationQ0)"/>
<parameter name =  "Rh1Mirror1CylPhiStartQ1" value="((-0.5*Rh1Mirror1NumCylAlongY*Rh1Mirror1CylPitch)/Rh1Mirror1CylRadialLocationQ1)"/>
<parameter name =  "Rh1Mirror1CylPhiStartQ2" value="((-0.5*Rh1Mirror1NumCylAlongY*Rh1Mirror1CylPitch)/Rh1Mirror1CylRadialLocationQ2)"/>
<parameter name =  "Rh1Mirror1CylPhiStartQ3" value="((-0.5*Rh1Mirror1NumCylAlongY*Rh1Mirror1CylPitch)/Rh1Mirror1CylRadialLocationQ3)"/>
<parameter name =  "Rh1Mirror1CylDeltaThetaQ0" value="Rh1Mirror1CylPitch/Rh1Mirror1CylRadialLocationQ0"/>
<parameter name =  "Rh1Mirror1CylDeltaThetaQ1" value="Rh1Mirror1CylPitch/Rh1Mirror1CylRadialLocationQ1"/>
<parameter name =  "Rh1Mirror1CylDeltaThetaQ2" value="Rh1Mirror1CylPitch/Rh1Mirror1CylRadialLocationQ2"/>
<parameter name =  "Rh1Mirror1CylDeltaThetaQ3" value="Rh1Mirror1CylPitch/Rh1Mirror1CylRadialLocationQ3"/>
<parameter name =  "Rh1Mirror1CylDeltaPhiQ0" value="Rh1Mirror1CylDeltaThetaQ0"/>
<parameter name =  "Rh1Mirror1CylDeltaPhiQ1" value="Rh1Mirror1CylDeltaThetaQ1"/>
<parameter name =  "Rh1Mirror1CylDeltaPhiQ2" value="Rh1Mirror1CylDeltaThetaQ2"/>
<parameter name =  "Rh1Mirror1CylDeltaPhiQ3" value="Rh1Mirror1CylDeltaThetaQ3"/>

<!-- Copy numbers for Rich1 Mirror1 -->
<parameter name="Rh1Mirror1Q0CopyNum" value="0"/>
<parameter name="Rh1Mirror1Q1CopyNum" value="1"/>
<parameter name="Rh1Mirror1Q2CopyNum" value="2"/>
<parameter name="Rh1Mirror1Q3CopyNum" value="3"/>
<parameter name="Rh1Mirror1CaFiInnerSegCopyNum" value="0"/>
<parameter name="Rh1Mirror1CaFiOuterSegCopyNum" value="1"/>

<!--        End   Rich1MirrorGeomParameters    -->
<!--   Now for user parameters -->
<parameter name="Rh1MaxExtentAlongX" value= "0.5*Rh1Mirror1MasterXSize"/>
<parameter name="Rh1MaxExtentAlongY" value="0.5*Rh1Mirror1MasterYSize"/>

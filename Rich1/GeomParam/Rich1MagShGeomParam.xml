<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- this file contains the geometry parameters
      for Rich1 Magnetic Shielding. The Master Volume near the photodetectors
      is made of Nitrogen. It has the shape of a (trapizoid - Box) .
      The Photodet support frame are kept inside this.
      The actual shields are kept are separate volumes
      inside the rich1submaster.

      The magnetic shield is composed of a (box-box) for the outer part,
      a box for the upstream part near aerogel, a box for the
      downstream part near the spherical Mirror1,
      a box on left extreme of Rich1 and a box on
      right extreme Rich1.


      The Top half is H0 and  the Bottom Half is H1.
      They are given copy numbers are 0 for Top and 1 for Bottom.
      For the shield boxes at the extreme the quadrants are  as looking
      downstream along Z,    Q0       Q3
                                beam
                             Q1       Q2

      Modified according to the drawings from IC in March 2004.
-->

<parameter name="Rh1MagShThickness" value="100.0*mm" />
<parameter name="Rh1MagShSmallThickness" value="50.0*mm" />
<!-- Now the smallest distance of the shield from beam at upstream, except for the teeth part -->
<parameter name="Rh1MgsUpstrYlim" value="550.0*mm"  />
<!-- Now the smallest distance of the shield from beam at downstream -->
<parameter name="Rh1MgsDnstrYlim" value="830.0*mm"  />
<!--  Now the location where the box-box part of shield ends
   <parameter name="Rh1ShRUpYLim" value="930.0*mm"  />
  -->
<parameter name="Rh1ShRUpYLim" value="Rh1MgsDnstrYlim"  />
<!-- Now the smallest distance of the teeth part of the shield from beam -->
<parameter name="Rh1MgsTeethUpstrYLim" value="350.0*mm" />
<!-- Now the bottom on the side plates. -->
<parameter name="Rh1MgsSideYLim" value="280.0*mm" />
<parameter name="Rh1MgsSideZMidLim"  value="1760.0*mm" />
<!-- Now the cut at the top of the two sides to help with
     hpd installation.   -->
<parameter name="Rh1MgsSideTopCutYLim" value="1600*mm" />
<parameter name="Rh1MgsSideTopCutZBeg" value="1577.5*mm" />
<parameter name="Rh1MgsSideTopCutZEnd" value="1977.5*mm" />
<parameter name="Rh1MgsSideTopCutXTolerence" value="80.0*mm" />
<!-- Now the edge towards middle of Rich1 in Z. This is 100 mm away in Y from the
     smallest distance from the beam pipe.
   This is point 8 in the drawings from Trevor   -->
<parameter name="Rh1MgsMidEdgeY" value="930.0*mm" />
<parameter name="Rh1MgsMidEdgeZ" value="1740.0*mm" />
<!--  The corner piece inside the box-box part of the shield.
  This is point 4 in the drawings from Trevor  -->
<parameter name="Rh1MgsCornerEdgeY" value="1350.0*mm" />
<parameter name="Rh1MgsCornerEdgeZ" value="1140.0*mm" />
<parameter name="Rh1MgsCornerThickness" value="100.0*mm" />
<parameter name="Rh1ShRDnstrSubBoxYTolerence" value="2.0*mm" />
<parameter name="Rh1ShRDnstrSubBoxZTolerence" value="10.0*mm" />
<parameter name="Rh1MgsCornerXTolerence" value="0.5*mm" />
<parameter name="Rh1MgsCornerYTolerence" value="0.5*mm" />
<parameter name="Rh1MgsCornerZTolerence" value="0.5*mm" />
<parameter name="Rh1MgsCornerDnsZTolerence" value="10.0*mm" />
<parameter name="Rh1MgsCornerDnsYTolerence" value="10.0*mm" />
<!-- Now for the part of Rich1 shield above and below TT.
   The following is point 13 from the drawings of Trevor.  -->
<parameter name="Rh1MgsDnsTTYP13Lim" value="1800.0*mm" />
<parameter name="Rh1MgsDnsTTYAddOnYThickness"  value="200*mm" />
<parameter name="Rh1MgsDnsTTZLim" value="2610.0*mm" />
<parameter name="Rh1MgsDnsTTXTolerence" value="0.5*mm" />
<parameter name="Rh1MgsDnsTTYTolerence" value="0.5*mm" />
<parameter name="Rh1MgsDnsTTZTolerence" value="0.5*mm" />
<parameter name="Rh1MgsMidPartZTolerence" value="1.0*mm" />
<!-- Now for the drawings of the Teeth part upstream .
  Point 1,2, 3 and 4 on the shield teeth drawings from Trevor.
  Use the convention that positive X goes to the left. -->
<parameter name="Rh1MgsTeethYTolerence" value="0.2*mm" />
<parameter name="Rh1MgsTeethP1CornerX" value="900.0*mm" />
<parameter name="Rh1MgsTeethP2CornerX" value="800.0*mm" />
<parameter name="Rh1MgsTeethP3CornerX" value="410.0*mm" />
<parameter name="Rh1MgsTeethP4CornerX" value="150.0*mm" />
<!--  Now for the derived parameters  -->
<!-- For the teeth part at the upstream.
   Each Teeth is a trapizoid - box. The trapozoid is
   defined by points 2,3,4 in the diagram of Trevor for the Teeth.
   The box is subtracted from the oblique side of the trapizoid
   which is farthest from the nominal beampipe.
   The teeth part on the four quadrants have the same shape, but
   positioned with the appropriate rotations and translations.
   The box boolean subtracted from the teeth part has its dimensions set
   a large value which is 10 meters. -->

<parameter name="Rh1MgsDnsTTYLim" value="Rh1MgsDnsTTYP13Lim-Rh1MgsDnsTTYAddOnYThickness" />
<parameter name="Rh1MgsTeethP1P2MidX" value="0.5*(Rh1MgsTeethP1CornerX+Rh1MgsTeethP2CornerX)" />
<parameter name="Rh1MgsTeethP2P3MidX" value="0.5*(Rh1MgsTeethP2CornerX+Rh1MgsTeethP3CornerX)" />
<parameter name="Rh1MgsTeethP1CornerY" value="Rh1MgsUpstrYlim-Rh1MgsTeethYTolerence" />
<parameter name="Rh1MgsTeethP2CornerY" value="Rh1MgsTeethUpstrYLim" />
<parameter name="Rh1MgsTeethP3CornerY" value="Rh1MgsTeethUpstrYLim" />
<parameter name="Rh1MgsTeethP4CornerY" value="Rh1MgsUpstrYlim-Rh1MgsTeethYTolerence" />
<parameter name="Rh1MgsTeethZThickness" value="Rh1MagShSmallThickness" />
<parameter name="Rh1MgsTeethP1P2MidY" value="0.5*(Rh1MgsTeethP1CornerY+Rh1MgsTeethP2CornerY)" />
<parameter name="Rh1MgsTeethTrapY1Size" value="Rh1MgsTeethP2CornerX-Rh1MgsTeethP3CornerX" />
<parameter name="Rh1MgsTeethTrapY2Size" value="Rh1MgsTeethTrapY1Size+2.0*(Rh1MgsTeethP3CornerX-Rh1MgsTeethP4CornerX)" />
<parameter name="Rh1MgsTeethTrapX1Size" value="Rh1MgsTeethZThickness" />
<parameter name="Rh1MgsTeethTrapX2Size" value="Rh1MgsTeethZThickness" />
<parameter name="Rh1MgsTeethTrapZSize" value="Rh1MgsTeethP1CornerY-Rh1MgsTeethP2CornerY" />
<parameter name="Rh1MgsTeethSubBoxXLargeSize" value="10.0*m"  />
<parameter name="Rh1MgsTeethSubBoxYLargeSize" value="10.0*m"  />
<parameter name="Rh1MgsTeethSubBoxZLargeSize" value="10.0*m"  />
<parameter name="Rh1MgsTeethP1ToP2Y" value="Rh1MgsTeethP1CornerX-Rh1MgsTeethP2CornerX" />
<parameter name="Rh1MgsTeethP1ToP2X" value="Rh1MgsTeethP1CornerY-Rh1MgsTeethP2CornerY" />
<parameter name="Rh1MgsTeethP1P2Tilt" value="(atan(Rh1MgsTeethP1ToP2Y/Rh1MgsTeethP1ToP2X))*rad" />
<parameter name="Rh1MgsTeethSubBoxRotX" value="-1.0*Rh1MgsTeethP1P2Tilt" />
<parameter name="Rh1MgsTeethSubBoxYShift" value="((Rh1MgsTeethP2CornerX-Rh1MgsTeethP2P3MidX
                                                  +0.5*Rh1MgsTeethSubBoxYLargeSize )/cos(Rh1MgsTeethSubBoxRotX))" />
<!-- Now to position the four teeths.   first the Z rot , then the X rot followed by the translations  -->
<parameter name="Rh1MgsTeethXQ0" value="Rh1MgsTeethP2P3MidX" />
<parameter name="Rh1MgsTeethYQ0" value="Rh1MgsTeethP1P2MidY" />
<parameter name="Rh1MgsTeethZQ0" value="Rh1SubMasterUpstrZLim+0.5*Rh1MgsTeethZThickness-Rh1SubMasterZ-Rich1MasterZ" />
<parameter name="Rh1MgsTeethRotZQ0" value="-(pi/2.0)*rad" />
<parameter name="Rh1MgsTeethRotXQ0" value="-(pi/2.0)*rad" />
<parameter name="Rh1MgsTeethXQ1" value="Rh1MgsTeethXQ0" />
<parameter name="Rh1MgsTeethYQ1" value="-1.0*Rh1MgsTeethYQ0" />
<parameter name="Rh1MgsTeethZQ1" value="Rh1MgsTeethZQ0" />
<parameter name="Rh1MgsTeethRotZQ1" value="-(pi/2.0)*rad" />
<parameter name="Rh1MgsTeethRotXQ1" value="(pi/2.0)*rad" />

<parameter name="Rh1MgsTeethXQ2" value="-1.0*Rh1MgsTeethXQ0"  />
<parameter name="Rh1MgsTeethYQ2" value="-1.0*Rh1MgsTeethYQ0" />
<parameter name="Rh1MgsTeethZQ2" value="Rh1MgsTeethZQ0" />
<parameter name="Rh1MgsTeethRotZQ2" value="(pi/2.0)*rad" />
<parameter name="Rh1MgsTeethRotXQ2" value="(pi/2.0)*rad" />

<parameter name="Rh1MgsTeethXQ3" value="-1.0*Rh1MgsTeethXQ0"  />
<parameter name="Rh1MgsTeethYQ3" value="Rh1MgsTeethYQ0" />
<parameter name="Rh1MgsTeethZQ3" value="Rh1MgsTeethZQ0" />
<parameter name="Rh1MgsTeethRotZQ3" value="(pi/2.0)*rad" />
<parameter name="Rh1MgsTeethRotXQ3" value="-(pi/2.0)*rad" />


<!-- For the big box-box part at the Y extremes
    This is actually box-box for getting the shell of
    a box. Then there are two more box subtractions of large boxes on the
    sides to get the cut where the hpds are moved in and out -->

<parameter name="Rh1MgsOuterBoxXSize" value="Rh1SubMasterXSize"  />
<parameter name="Rh1MgsOuterBoxYSize" value="0.5*Rh1SubMasterYSize-Rh1ShRUpYLim"  />
<parameter name="Rh1MgsOuterBoxZSize" value="Rh1SubMasterZSize"  />
<parameter name="Rh1MgsInnerBoxXSize" value="Rh1MgsOuterBoxXSize-2*Rh1MagShThickness"  />
<!-- Make the following YSixe a large value for boolean subtraction -->
<parameter name="Rh1MgsInnerBoxYSize" value="Rh1SubMasterYSize"  />
<parameter name="Rh1MgsInnerBoxZSize" value="Rh1SubMasterZSize-Rh1MagShSmallThickness-Rh1MagShThickness"  />
<parameter name="Rh1MgsOuterBoxY"     value="Rh1ShRUpYLim+0.5*Rh1MgsOuterBoxYSize"  />
<parameter name="Rh1MgsOuterBoxYH0"   value="Rh1MgsOuterBoxY"  />
<parameter name="Rh1MgsOuterBoxYH1"   value="-1.0*Rh1MgsOuterBoxY"  />
<parameter name="Rh1MgsInnerBoxY"     value="0.5*(Rh1MgsOuterBoxYSize-Rh1MgsInnerBoxYSize)-Rh1MagShThickness"  />
<parameter name="Rh1MgsInnerBoxYH0"   value="Rh1MgsInnerBoxY"  />
<parameter name="Rh1MgsInnerBoxYH1"   value="-1.0*Rh1MgsInnerBoxY"  />
<parameter name="Rh1MgsInnerBoxZ"     value="0.5*(Rh1MagShSmallThickness-Rh1MagShThickness)" />
<parameter name="Rh1MgsTopCutLargeBoxLargeXSize" value="10.0*m" />
<parameter name="Rh1MgsTopCutLargeBoxLargeYSize" value="10.0*m" />
<parameter name="Rh1MgsTopCutLargeBoxZSize" value="Rh1MgsSideTopCutZEnd-Rh1MgsSideTopCutZBeg" />
<parameter name="Rh1MgsTopCutLargeBoxX" value="0.5*Rh1MgsOuterBoxXSize-Rh1MagShThickness+Rh1MgsSideTopCutXTolerence+0.5*Rh1MgsTopCutLargeBoxLargeXSize" />
<parameter name="Rh1MgsTopCutLargeBoxY" value="Rh1MgsSideTopCutYLim-Rh1MgsOuterBoxY+0.5*Rh1MgsTopCutLargeBoxLargeYSize" />
<parameter name="Rh1MgsTopCutLargeBoxZ" value="0.5*(Rh1MgsSideTopCutZEnd+Rh1MgsSideTopCutZBeg)-Rh1SubMasterZ-Rich1MasterZ" />
<parameter name="Rh1MgsTopCutLargeBoxXQ0" value="Rh1MgsTopCutLargeBoxX" />
<parameter name="Rh1MgsTopCutLargeBoxXQ1" value="Rh1MgsTopCutLargeBoxX" />
<parameter name="Rh1MgsTopCutLargeBoxXQ2" value="-1.0*Rh1MgsTopCutLargeBoxX" />
<parameter name="Rh1MgsTopCutLargeBoxXQ3" value="-1.0*Rh1MgsTopCutLargeBoxX" />
<parameter name="Rh1MgsTopCutLargeBoxYQ0" value="Rh1MgsTopCutLargeBoxY" />
<parameter name="Rh1MgsTopCutLargeBoxYQ1" value="-1.0*Rh1MgsTopCutLargeBoxY" />
<parameter name="Rh1MgsTopCutLargeBoxYQ2" value="-1.0*Rh1MgsTopCutLargeBoxY" />
<parameter name="Rh1MgsTopCutLargeBoxYQ3" value="Rh1MgsTopCutLargeBoxY" />


<!-- Now for the upstream part  -->
<parameter name="Rh1MgsUpstrBoxXSize" value="Rh1MgsOuterBoxXSize"   />
<parameter name="Rh1MgsUpstrBoxYSize" value="Rh1ShRUpYLim-Rh1MgsUpstrYlim"   />
<parameter name="Rh1MgsUpstrBoxZSize" value="Rh1MagShSmallThickness"   />
<parameter name="Rh1MgsUpstrBoxY" value="Rh1MgsUpstrYlim+0.5*Rh1MgsUpstrBoxYSize"   />
<parameter name="Rh1MgsUpstrBoxYH0" value="Rh1MgsUpstrBoxY"   />
<parameter name="Rh1MgsUpstrBoxYH1" value="-1.0*Rh1MgsUpstrBoxY"   />
<parameter name="Rh1MgsUpstrBoxZ" value="Rh1SubMasterUpstrZLim+0.5*Rh1MgsUpstrBoxZSize-Rh1SubMasterZ-Rich1MasterZ"   />
<!-- Now for the part downstream of the Rich1SubMaster and above and below TT . It is placed in
     Rich1Master. It has a box cut out. Hence it is a box-box.  -->
<parameter name="Rh1MgsDnsTTXSize" value="Rh1MgsOuterBoxXSize-Rh1MgsDnsTTXTolerence" />
<parameter name="Rh1MgsDnsTTYSize" value="Rh1SubMasterYLim-Rh1MgsDnsTTYLim-Rh1MgsDnsTTYTolerence" />
<parameter name="Rh1MgsDnsTTZSize"  value="Rh1MgsDnsTTZLim-Rh1SubMasterDnstrZLim-Rh1MgsDnsTTZTolerence" />
<parameter name="Rh1MgsDnsTTY"  value="Rh1MgsDnsTTYLim+0.5*Rh1MgsDnsTTYSize" />
<parameter name="Rh1MgsDnsTTYH0" value="Rh1MgsDnsTTY" />
<parameter name="Rh1MgsDnsTTYH1" value="-1.0*Rh1MgsDnsTTY" />
<parameter name="Rh1MgsDnsTTZ"  value="Rh1SubMasterDnstrZLim+Rh1MgsDnsTTZTolerence+0.5*Rh1MgsDnsTTZSize-Rich1MasterZ" />
<parameter name="Rh1MgsDnsTTInnerSubBoxLargeXSize" value="10.0*m"  />
<parameter name="Rh1MgsDnsTTInnerSubBoxLargeYSize" value="10.0*m"  />
<parameter name="Rh1MgsDnsTTInnerSubBoxZSize" value="Rh1MgsDnsTTZSize-Rh1MagShThickness" />
<parameter name="Rh1MgsDnsTTInnerSubBoxYShift" value="-0.5*(Rh1MgsDnsTTYSize+Rh1MgsDnsTTInnerSubBoxLargeYSize)+Rh1MgsDnsTTYAddOnYThickness" />
<parameter name="Rh1MgsDnsTTInnerSubBoxZShift"  value="0.5*Rh1MgsDnsTTZSize-Rh1MagShThickness-0.5*Rh1MgsDnsTTInnerSubBoxZSize" />
<parameter name="Rh1MgsDnsTTInnerSubBoxYShiftH0" value="Rh1MgsDnsTTInnerSubBoxYShift" />
<parameter name="Rh1MgsDnsTTInnerSubBoxYShiftH1" value="-1.0*Rh1MgsDnsTTInnerSubBoxYShift" />
<!-- Now for the part going towards the middle of Rich1.  -->
<parameter name="Rh1MgsMidPartBoxXSize"   value="Rh1MgsOuterBoxXSize-2.0*Rh1MagShThickness"  />
<parameter name="Rh1MgsMidPartBoxYSize"   value="Rh1MagShThickness" />
<parameter name="Rh1MgsMidPartBoxZSize"   value="Rh1SubMasterDnstrZLim-Rh1MagShThickness-Rh1MgsMidPartZTolerence-Rh1MgsMidEdgeZ" />
<parameter name="Rh1MgsMidPartBoxY"  value="Rh1MgsDnstrYlim+0.5*Rh1MgsMidPartBoxYSize" />
<parameter name="Rh1MgsMidPartBoxYH0" value="Rh1MgsMidPartBoxY" />
<parameter name="Rh1MgsMidPartBoxYH1" value="-1.0*Rh1MgsMidPartBoxY" />
<parameter name="Rh1MgsMidPartBoxZ" value="Rh1SubMasterDnstrZLim-Rh1MagShThickness-Rh1MgsMidPartZTolerence-0.5*Rh1MgsMidPartBoxZSize-Rh1SubMasterZ-Rich1MasterZ" />
<!--  Now for the boxes at the Sides on the left extreme and the right extreme. subtract a
      trapozoid fromt his box to get the shape at the places nearest to middle of the detector. -->
<parameter name="Rh1MgsSideBoxXSize"  value="Rh1MagShThickness" />
<parameter name="Rh1MgsSideBoxYSize"  value="Rh1ShRUpYLim-Rh1MgsSideYLim" />
<parameter name="Rh1MgsSideBoxZSize"  value="Rh1SubMasterZSize-Rh1MagShSmallThickness" />
<parameter name="Rh1MgsSideZLargeSize" value="5.0*m"  />
<parameter name="Rh1MgsSideSubTrapX1LargeSize" value="5.0*m" />
<parameter name="Rh1MgsSideSubTrapX2LargeSize" value="5.0*m" />
<parameter name="Rh1MgsSideSubTrapY2SmallSize" value= "100.0*mm" />
<parameter name="Rh1MgsSideSubTrapY1Size" value= "Rh1MgsSideSubTrapY2SmallSize+
                                2.0*(Rh1MgsUpstrYlim-Rh1MgsSideYLim)*
                                Rh1MgsSideZLargeSize/(Rh1MgsSideZMidLim-(Rh1SubMasterUpstrZLim+Rh1MagShSmallThickness))" />
<parameter name="Rh1MgsSideSubTrapY" value="-0.5*(Rh1MgsSideBoxYSize+Rh1MgsSideSubTrapY2SmallSize)" />
<parameter name="Rh1MgsSideSubTrapZ" value="0.5*Rh1MgsSideBoxZSize-(Rh1SubMasterDnstrZLim-Rh1MgsSideZMidLim)-0.5*Rh1MgsSideZLargeSize" />
<parameter name="Rh1MgsSideBoxX"  value="0.5*(Rh1SubMasterXSize-Rh1MgsSideBoxXSize)" />
<parameter name="Rh1MgsSideBoxXQ0" value="Rh1MgsSideBoxX" />
<parameter name="Rh1MgsSideBoxXQ1" value="Rh1MgsSideBoxX" />
<parameter name="Rh1MgsSideBoxXQ2" value="-1.0*Rh1MgsSideBoxX" />
<parameter name="Rh1MgsSideBoxXQ3" value="-1.0*Rh1MgsSideBoxX" />
<parameter name="Rh1MgsSideBoxY"   value="Rh1MgsSideYLim+0.5*Rh1MgsSideBoxYSize"  />
<parameter name="Rh1MgsSideBoxYQ0"   value="Rh1MgsSideBoxY" />
<parameter name="Rh1MgsSideBoxYQ1"   value="-1.0*Rh1MgsSideBoxY" />
<parameter name="Rh1MgsSideBoxYQ2"   value="-1.0*Rh1MgsSideBoxY" />
<parameter name="Rh1MgsSideBoxYQ3"   value="Rh1MgsSideBoxY" />
<parameter name="Rh1MgsSideBoxZ"     value="0.5*Rh1MagShSmallThickness" />
<parameter name="Rh1MgsSideBoxRotZQ0" value="0.0*rad" />
<parameter name="Rh1MgsSideBoxRotZQ1" value="pi*rad" />
<parameter name="Rh1MgsSideBoxRotZQ2" value="pi*rad" />
<parameter name="Rh1MgsSideBoxRotZQ3" value="0.0*rad" />
<!-- Now for the corner pieces at the upstream end. These are simple boxes.-->
<parameter name="Rh1MgsCornerXSize" value="Rh1MgsOuterBoxXSize-2.0*Rh1MagShThickness-Rh1MgsCornerXTolerence" />
<parameter name="Rh1MgsCornerYSize" value="0.5*Rh1SubMasterYSize-Rh1MagShThickness-Rh1MgsCornerEdgeY-Rh1MgsCornerYTolerence" />
<parameter name="Rh1MgsCornerZSize" value="Rh1MgsCornerThickness-Rh1MgsCornerZTolerence" />
<parameter name="Rh1MgsCornerY"  value="Rh1MgsCornerEdgeY+0.5*Rh1MgsCornerYSize" />
<parameter name="Rh1MgsCornerYH0"  value="Rh1MgsCornerY" />
<parameter name="Rh1MgsCornerYH1"  value="-1.0*Rh1MgsCornerY" />
<parameter name="Rh1MgsCornerZ"  value="Rh1SubMasterUpstrZLim+Rh1MagShSmallThickness+Rh1MgsCornerZTolerence+
                                        0.5*Rh1MgsCornerZSize-Rh1SubMasterZ-Rich1MasterZ" />


<!--  Now for the region inside the shield which has the photodetector support frame -->
<!-- This is a trapizioid - box at the Ylimit fartherest from beamline  to match the shape of the
     Magnetic shield. A second box is subtracted at  downstream to avoid the part of the shielding
     with shelf. The upstream part of the box is just after the iron piece which is at the
     upstream top corner in the top half and the upstream bottom corner for the bottom half.
     A third box is subtracted to avoid any overlap with the corner pieces of the shield.
     The tapizoid starts at mid point of 3 and 4  and passes through the mid point of 8 and 9
     in the drawing of Trevor.(except for the tolerence values of these shield pieces. )  -->

<parameter name="Rh1ShRTrapXSize" value="Rh1SubMasterXSize-2.0*Rh1MagShThickness" />
<parameter name="Rh1ShRTrapX1Size" value="Rh1ShRTrapXSize" />
<parameter name="Rh1ShRTrapX2Size" value="Rh1ShRTrapXSize" />
<parameter name="Rh1ShrTrapYShiftTolerenceFromP3P4" value="75.0*mm" />
<parameter name="Rh1ShrTrapZShiftTolerenceFromP8P9" value="75.0*mm" />
<parameter name="Rh1ShrTrapMidOfP3P4Z" value="Rh1SubMasterUpstrZLim+Rh1MagShSmallThickness+Rh1MgsCornerZTolerence+0.5*Rh1MgsCornerThickness" />
<parameter name="Rh1ShrTrapMidOfP3P4Y" value="Rh1MgsCornerEdgeY-Rh1ShrTrapYShiftTolerenceFromP3P4" />
<parameter name="Rh1ShrTrapMidOfP8P9Z" value="Rh1MgsMidEdgeZ-Rh1ShrTrapZShiftTolerenceFromP8P9" />
<parameter name="Rh1ShrTrapMidOfP8P9Y" value="Rh1MgsMidEdgeY-0.5*Rh1MagShThickness" />
<parameter name="Rh1ShRDnZLim" value="Rh1SubMasterDnstrZLim-Rh1MagShThickness"  />
<parameter name="Rh1ShRUpZLim" value="Rh1ShrTrapMidOfP3P4Z"  />
<parameter name="Rh1ShRTrapZSize" value="Rh1ShRDnZLim-Rh1ShRUpZLim" />
<parameter name="Rh1ShRTrapYOuterLim"  value="0.5*Rh1SubMasterYSize-Rh1MagShThickness"  />
<parameter name="Rh1ShRTrapYDiffToMid" value="Rh1ShrTrapMidOfP3P4Y-Rh1ShrTrapMidOfP8P9Y" />
<parameter name="Rh1ShRTrapZDiffToMid" value="Rh1ShrTrapMidOfP8P9Z-Rh1ShRUpZLim" />
<parameter name="Rh1ShRTrapYDiffToTopOfMidShelf" value="Rh1ShrTrapMidOfP3P4Y-Rh1MgsMidEdgeY" />
<parameter name="Rh1ShRTrapYTotalAbsDiff" value="(Rh1ShRTrapYDiffToMid/Rh1ShRTrapZDiffToMid)*Rh1ShRTrapZSize" />
<parameter name="Rh1ShRTrapY1Size" value="Rh1MgsCornerYSize+Rh1ShrTrapYShiftTolerenceFromP3P4"  />
<parameter name="Rh1ShRTrapY2Size" value="Rh1ShRTrapY1Size+2.0*Rh1ShRTrapYTotalAbsDiff"  />
<!--  Create large boxes for boolean subtractions -->
<parameter name="Rh1ShRSubBoxXSize"   value="5.0*m"  />
<parameter name="Rh1ShRSubBoxYSize"   value="5.0*m"  />
<parameter name="Rh1ShRSubBoxZSize"   value="5.0*m"  />

<parameter name="Rh1ShRTrapX"  value="0.0*mm"  />
<parameter name="Rh1ShRTrapY"  value="Rh1ShRTrapYOuterLim-0.5*Rh1ShRTrapY1Size"  />
<parameter name="Rh1ShRTrapZ"  value="0.5*(Rh1ShRDnZLim+Rh1ShRUpZLim)-Rh1SubMasterZ-Rich1MasterZ"  />

<!--  First for the +Y most and -Y most part of the shield. -->
<parameter name="Rh1ShRTrapXH0" value="Rh1ShRTrapX"  />
<parameter name="Rh1ShRTrapYH0" value="Rh1ShRTrapY"  />
<parameter name="Rh1ShRTrapZH0"  value="Rh1ShRTrapZ"  />

<parameter name="Rh1ShRSubBoxXH0"  value="0.0*mm"  />
<parameter name="Rh1ShRSubBoxYH0"  value="0.5*(Rh1ShRTrapY1Size+Rh1ShRSubBoxYSize)"  />
<parameter name="Rh1ShRSubBoxZH0"  value="0.0*mm"  />

<parameter name="Rh1ShRTrapXH1"  value="Rh1ShRTrapX"  />
<parameter name="Rh1ShRTrapYH1"  value="-1.0*Rh1ShRTrapY"  />
<parameter name="Rh1ShRTrapZH1"  value="Rh1ShRTrapZ"  />
<parameter name="Rh1ShRSubBoxXH1"  value="Rh1ShRSubBoxXH0"  />
<parameter name="Rh1ShRSubBoxYH1"  value="-1.0*Rh1ShRSubBoxYH0"  />
<parameter name="Rh1ShRSubBoxZH1"  value="Rh1ShRSubBoxZH0"  />

<!-- Now for the shelf part of shield downstream  -->
<parameter name="Rh1ShRDnstrSubBoxXSize"   value="Rh1ShRSubBoxXSize"  />
<parameter name="Rh1ShRDnstrSubBoxYSize"   value="Rh1ShRSubBoxYSize"  />
<parameter name="Rh1ShRDnstrSubBoxZSize"   value="Rh1ShRSubBoxZSize"  />

<parameter name="Rh1ShRDnstrSubBoxXH0"  value="0.0*mm"  />
<parameter name="Rh1ShRDnstrSubBoxYH0"  value="-0.5*Rh1ShRTrapY1Size- Rh1ShRTrapYDiffToTopOfMidShelf+Rh1ShRDnstrSubBoxYTolerence-0.5*Rh1ShRDnstrSubBoxYSize"  />
<parameter name="Rh1ShRDnstrSubBoxZH0"  value="0.5*Rh1ShRTrapZSize-(Rh1ShRDnZLim-Rh1ShrTrapMidOfP8P9Z-Rh1ShrTrapZShiftTolerenceFromP8P9)-Rh1ShRDnstrSubBoxZTolerence+0.5*Rh1ShRDnstrSubBoxZSize"  />
<parameter name="Rh1ShRDnstrSubBoxXH1"  value="Rh1ShRDnstrSubBoxXH0"  />
<parameter name="Rh1ShRDnstrSubBoxYH1"  value="-1.0*Rh1ShRDnstrSubBoxYH0"  />
<parameter name="Rh1ShRDnstrSubBoxZH1"  value="Rh1ShRDnstrSubBoxZH0"  />

<!-- Now for the corner part upstream -->
<parameter name="Rh1ShRUpstrCornerSubBoxXSize"   value="Rh1ShRSubBoxXSize"  />
<parameter name="Rh1ShRUpstrCornerSubBoxYSize"   value="Rh1ShRSubBoxYSize"  />
<parameter name="Rh1ShRUpstrCornerSubBoxZSize"   value="Rh1ShRSubBoxZSize"  />

<parameter name="Rh1ShRUpstrCornerSubBoxXH0"  value="0.0*mm"  />
<parameter name="Rh1ShRUpstrCornerSubBoxYH0"  value ="-0.5*Rh1ShRTrapY1Size+Rh1ShrTrapYShiftTolerenceFromP3P4+0.5*Rh1ShRUpstrCornerSubBoxYSize-Rh1MgsCornerDnsYTolerence" />
<parameter name="Rh1ShRUpstrCornerSubBoxZH0"  value="-0.5*Rh1ShRTrapZSize+0.5*Rh1MgsCornerThickness-0.5*Rh1ShRUpstrCornerSubBoxZSize+Rh1MgsCornerDnsZTolerence" />

<parameter name="Rh1ShRUpstrCornerSubBoxXH1"  value="Rh1ShRUpstrCornerSubBoxXH0"  />
<parameter name="Rh1ShRUpstrCornerSubBoxYH1" value ="-1.0*Rh1ShRUpstrCornerSubBoxYH0" />
<parameter name="Rh1ShRUpstrCornerSubBoxZH1"  value="Rh1ShRUpstrCornerSubBoxZH0" />


<!--  Now for the parameters derived for Brunel -->

<parameter name="C4F10RadiatorXMaxExtentBegin" value="0.5*Rh1SubMasterXSize-Rh1MagShThickness" />
<parameter name="C4F10RadiatorXMaxExtentEnd" value="0.5*Rh1SubMasterXSize-Rh1MagShThickness" />
<parameter name="C4F10RadiatorYMaxExtentBegin" value="Rh1MgsCornerEdgeY" />
<parameter name="C4F10RadiatorYMaxExtentEnd" value="Rh1MgsMidEdgeY" />
<!-- End of Mag Shield Parameters  -->












<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DDDB SYSTEM "git:/DTD/geometry.dtd">
<DDDB>
<!-- """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" -->
<!--
  CVS tag: Name: DC06-pre1 $
  Log: Coil.xml,v $
  Revision 1.1.1.1  2007/01/30 13:34:59  marcocle
  CVS based back up for the SQLite conditions database

  Revision 1.4  2006/02/06 15:54:04  cattanem
  DC06 geometry and cleanup of older stuff

  Revision 1.3  2002/09/12 09:33:02  cattanem
  Add vertical Rich, Velo optimisation

  Revision 1.2  2002/06/04 16:23:56  cattanem
  changes from Galina

  Revision 1.1  2002/03/13 12:13:07  ibelyaev
   new description of Magnet by Galina Pakhlova

-->
<!-- """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""" -->
<!-- ******************************************************************** -->
<!--                             Coil  Geometry                           -->
<!-- ******************************************************************** -->

<!-- %%% Parameters of Coil subtractions %%% -->

<!--  XZ plate: unbended "pancake" basic trapezia (x_half_size) -->

<parameter name  = "CoilOutRadius"  value = "CoilInnRadius+CoilWidth"/>
<parameter name  = "CoilAngle"      value = "acos(CoilInnZ/CoilLatLength)"/>
<parameter name  = "TrapUpX"
           value = "CoilCylX1+(CoilInnRadius+CoilWidth)*tan(45*degree-0.5*CoilAngle)"/>
<parameter name  = "TrapLowX"
           value = "CoilCylX2+(CoilInnRadius+CoilWidth)*tan(45*degree+0.5*CoilAngle)"/>

<!-- basic bended trapezoid (before subtractions) -->

 <parameter name = "CoilHoleZ"     value = "CoilInnZ+2*CoilInnRadius"/>
 <parameter name = "CoilUpDeltaX"  value = "CoilWidth*tan(CoilBendLine)"/>
 <parameter name = "CoilLowDeltaX" value = "(CoilHoleZ+CoilWidth)*tan(CoilBendLine)"/>

<!-- inner subtracted trapezoid -->

  <parameter name = "InnBendUpX"      value = "CoilS1-CoilUpDeltaX"/>
  <parameter name = "InnBendLowX"     value = "CoilS1+CoilLowDeltaX"/>

<!--  "inner" plate of bended "pancake" -->

<parameter name  = "PlateUpY"   value = "(TrapUpX-InnBendUpX)*cos(45*degree)"/>
<parameter name  = "PlateLowY"  value = "(TrapLowX-InnBendLowX)*cos(45*degree)"/>

<!--  lateral angle of vertical cut   -->

 <parameter name  = "CutAngle"
            value = "atan((InnBendLowX+PlateLowY-InnBendUpX-PlateUpY)/CoilSizeZ)"/>

<!-- parameters of  cylindrical hole bending -->

<parameter name  = "UpCylDeltaX"   value = "(CoilCylX1-CoilS5)*(1-cos(45*degree))"/>
<parameter name  = "LowCylDeltaX"
           value = "(CoilCylX2-CoilS5-CoilInnZ*tan(CoilBendLine))*(1-cos(45*degree))"/>

<!-- "lateral box" bending -->

<parameter name  = "BoxDeltaX"     value = "0.5*(UpCylDeltaX+LowCylDeltaX)"/>

<!-- tolerances  -->
   <parameter name  = "CoilTolerance"         value = "1*mm"/>
   <parameter name  = "TolAngle1"             value = "2.1*degree"/>
   <parameter name  = "TolAngle2"             value = "2*degree"/>
   <parameter name  = "Delta"                 value = "250*mm"/>

<!-- ************************************************************************ -->
<!-- ************************************************************************ -->
  <logvol name = "Coil"   material = "Magnet/CoilAl">
        <subtraction name = "Coil_Subtraction">

<!-- % -Z((X1,X2)Y1) % -->
<!-- % +Z((X3,X4)Y2) % -->
      <trap name   = "Basic_Trapezoid"
            sizeZ  = "CoilSizeZ"
                sizeY1 = "2*CoilSizeY"
                sizeX1 = "2*(CoilS5-CoilUpDeltaX+2*CoilSizeY)"
                sizeX2 = "2*(CoilS5-CoilUpDeltaX)"
                    sizeY2 = "2*CoilSizeY"
                    sizeX3 = "2*(CoilS5+CoilLowDeltaX+2*CoilSizeY)"
                    sizeX4 = "2*(CoilS5+CoilLowDeltaX)"/>

<!-- ************************************************************************ -->
      <trap name   = "Inner_Cutout"
            sizeZ  = "CoilSizeZ+CoilTolerance"
                sizeY1 = "2*CoilSizeY"
                sizeX1 = "2*(InnBendUpX+2*CoilSizeY)"
                sizeX2 = "2*InnBendUpX"
                    sizeY2 = "2*CoilSizeY"
                    sizeX3 = "2*(InnBendLowX+2*CoilSizeY)"
                    sizeX4 = "2*InnBendLowX"/>
           <posXYZ y = "-1*CoilThick"/>


<!-- ************************************************************************ -->
      <box  name  = "Vertical_Cut_Right"
            sizeX =  "2*CoilSizeX"
            sizeY =  "5*CoilSizeY"
            sizeZ =  "4*CoilSizeZ"/>
          <posXYZ    z = "0.5*CoilSizeZ-CoilSizeX*sin(CutAngle)"
                     x = "InnBendLowX+PlateLowY+CoilSizeX*cos(CutAngle)"/>
          <rotXYZ rotY = "CutAngle"/>

      <box  name   = "Vertical_Cut_Left"
            sizeX =  "2*CoilSizeX"
            sizeY =  "5*CoilSizeY"
            sizeZ =  "4*CoilSizeZ"/>
          <posXYZ    z = "0.5*CoilSizeZ-CoilSizeX*sin(CutAngle)"
                     x = "-InnBendLowX-PlateLowY-CoilSizeX*cos(CutAngle)"/>
          <rotXYZ rotY = "-CutAngle"/>

<!-- ************************************************************************ -->

 <box  name = "Vertical_Cut_Right_Low_Delta"
      sizeX =  "2*CoilOutRadius*(1/sin(45*degree-0.5*CoilAngle)-1)*cos(45*degree)"
      sizeY =  "5*CoilSizeY"
      sizeZ =  "4*CoilSizeZ"/>
          <posXYZ    z = "0.5*CoilSizeZ"
                     x = "InnBendLowX+PlateLowY"/>
          <rotXYZ rotY = "-45*degree+0.5*CoilAngle"/>

 <box  name  = "Vertical_Cut_Left_Low_Delta"
       sizeX =  "2*CoilOutRadius*(1/sin(45*degree-0.5*CoilAngle)-1)*cos(45*degree)"
       sizeY =  "5*CoilSizeY"
       sizeZ =  "4*CoilSizeZ"/>
           <posXYZ    z = "0.5*CoilSizeZ"
                      x = "-InnBendLowX-PlateLowY"/>
           <rotXYZ rotY = "45*degree-0.5*CoilAngle"/>

 <box  name   = "Vertical_Cut_Right_Up_Delta"
        sizeX =  "2*CoilOutRadius*(1/sin(45*degree+0.5*CoilAngle)-1)*0.9"
        sizeY =  "5*CoilSizeY"
        sizeZ =  "4*CoilSizeZ"/>
            <posXYZ   z = "-0.5*CoilSizeZ"
                      x = "InnBendUpX+PlateUpY"/>
            <rotXYZ rotY = "-135*degree-0.5*CoilAngle"/>

 <box  name   = "Vertical_Cut_Left_Up_Delta"
        sizeX =  "2*CoilOutRadius*(1/sin(45*degree+0.5*CoilAngle)-1)*0.9"
        sizeY =  "5*CoilSizeY"
        sizeZ =  "4*CoilSizeZ"/>
            <posXYZ    z = "-0.5*CoilSizeZ"
                       x = "-InnBendUpX-PlateUpY"/>
            <rotXYZ rotY = "135*degree+0.5*CoilAngle"/>

<!-- ************************************************************************ -->

   <trap name  = "Central_Hole"
         sizeZ =  "CoilHoleZ"
             sizeY1 = "7*CoilSizeY"
             sizeX1 = "2*(CoilS5+(CoilCylX1-CoilS5)*cos(45*degree))"
             sizeX2 = "2*(CoilS5+(CoilCylX1-CoilS5)*cos(45*degree))"
                 sizeY2 = "7*CoilSizeY"
                 sizeX3 = "2*(CoilS5+(CoilCylX2-CoilS5)*cos(45*degree))+Delta"
                 sizeX4 = "2*(CoilS5+(CoilCylX2-CoilS5)*cos(45*degree))+Delta"/>


<!-- ************************************************************************ -->
   <tubs name = "Upper_Right_Cylinder"
         sizeZ       = "4.1*CoilSizeY"
         outerRadius = "CoilInnRadius+CoilTolerance"/>
           <posXYZ    x = "CoilCylX1-UpCylDeltaX"
                      z = "-0.5*CoilInnZ"/>
           <rotXYZ rotX = "90*degree"/>

    <tubs name = "Lower_Right_Cylinder"
         sizeZ       = "4.1*CoilSizeY"
         outerRadius = "CoilInnRadius"/>
           <posXYZ    x = "CoilCylX2-LowCylDeltaX"
                      z = "0.5*CoilInnZ"/>
           <rotXYZ rotX = "90*degree"/>

   <tubs name = "Upper_Left_Cylinder"
         sizeZ       = "4*CoilSizeY"
         outerRadius = "CoilInnRadius+CoilTolerance"/>
           <posXYZ    x = "-CoilCylX1+UpCylDeltaX"
                      z = "-0.5*CoilInnZ"/>
           <rotXYZ rotX = "90*degree"/>

    <tubs name = "Lower_Left_Cylinder"
         sizeZ       = "4*CoilSizeY"
         outerRadius = "CoilInnRadius"/>
           <posXYZ    x = "-CoilCylX2+LowCylDeltaX"
                      z = "0.5*CoilInnZ"/>
          <rotXYZ rotX = "90*degree"/>


<!-- ************************************************************************ -->
    <box name  = "Hole_Right_Box"
         sizeX = "2*CoilInnRadius"
         sizeY = "7*CoilSizeY"
         sizeZ = "CoilLatLength+CoilTolerance"/>
            <posXYZ    x = "0.5*(CoilCylX1+CoilCylX2)-BoxDeltaX"/>
            <rotXYZ rotY = "CoilAngle-TolAngle2"/>

    <box name = "Hole_Left_Box"
         sizeX = "2*CoilInnRadius"
         sizeY = "7*CoilSizeY"
         sizeZ = "CoilLatLength+CoilTolerance"/>
            <posXYZ    x = "-0.5*(CoilCylX1+CoilCylX2)+BoxDeltaX"/>
            <rotXYZ rotY = "-CoilAngle+TolAngle2"/>

<!-- ************************************************************************ -->
  </subtraction>
 </logvol>
<!-- ************************************************************************ -->
</DDDB>

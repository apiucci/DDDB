<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE DDDB SYSTEM "git:/DTD/geometry.dtd">

<DDDB>

  <!-- ***************************************************************** -->
  <!-- *  Description of the OT modules. There are 4 types of          * -->
  <!-- *  modules:                                                     * -->
  <!-- *  - Long modules (L)                                           * -->
  <!-- *  - Short modules (S1, S2, and S3)                             * -->
  <!-- *                                                               * -->
  <!-- *          _____________________________________________        * -->
  <!-- *         |          |   |   |   | |   |   |            |       * -->
  <!-- *         |          |   |   |S2 | |   |   |            |       * -->
  <!-- *         |          |   |   |   |S|   |   |   OT       |       * -->
  <!-- *         |          | L |S1 |   |3|S1 | L |            |       * -->
  <!-- *         |          |   |   |___|_|   |   |            |       * -->
  <!-- *         |          |   |___|     |___|   |            |       * -->
  <!-- *         |   L      |===|___  IT   ___|===|    L       |       * -->
  <!-- *         |          |   |   |___ _|   |   |            |       * -->
  <!-- *         |          |   |   |   | |   |   |            |       * -->
  <!-- *         |          |   |   |S2 | |   |   |            |       * -->
  <!-- *         |          | L |S1 |   |S|S1 | L |            |       * -->
  <!-- *         |          |   |   |   |3|   |   |            |       * -->
  <!-- *         |__________|___|___|___|_|___|___|____________|       * -->
  <!-- *                                                               * -->
  <!-- * The modules consist of a passive (P) and an active (A) plane. * -->
  <!-- * The active part is defined as a SensDet.                      * -->
  <!-- *                                                               * -->
  <!-- *  Date: 01-12-2003                                             * -->
  <!-- *  Authors: Jacopo Nardulli & Jeroen van Tilburg                * -->
  <!-- ***************************************************************** -->


  <catalog name="Modules">
    <logvolref href="#lvLModule"        />
    <logvolref href="#lvLModuleA"       />
    <logvolref href="#lvLModuleP"       />
    <logvolref href="#lvLSideWall"      />
    <logvolref href="#lvS1Module"       />
    <logvolref href="#lvS1ModuleA"      />
    <logvolref href="#lvS1ModuleP"      />
    <logvolref href="#lvS1SideWall"     />
    <logvolref href="#lvS2Module"       />
    <logvolref href="#lvS2ModuleA"      />
    <logvolref href="#lvS2ModuleP"      />
    <logvolref href="#lvS2SideWall"     />
    <logvolref href="#lvS3Module"       />
    <logvolref href="#lvS3ModuleA"      />
    <logvolref href="#lvS3ModuleP"      />
    <logvolref href="#lvS3SideWall"     />
  </catalog>


  <!-- ************************ -->
  <!-- * Define the L module  * -->
  <!-- ************************ -->

  <logvol name="lvLModuleA"
          material="OT/OTActiveMix"
          sensdet="GiGaSensDetTracker/OTSDet">
    <box name="LModABox"
         sizeX="LModXSize-2.0*SideWallThick"
         sizeY="0.5*LModYSize"
         sizeZ="ActiveThick" />
  </logvol>

  <logvol name="lvLModuleP" material="OT/OTPassiveMix">
    <box name="LModPBox"
         sizeX="LModXSize-2.0*SideWallThick"
         sizeY="0.5*LModYSize"
         sizeZ="PassiveThick" />
  </logvol>

  <logvol name="lvLSideWall" material="OT/OTSideWallMix">
    <box name="LSideWallBox"
         sizeX="SideWallThick"
         sizeY="0.5*LModYSize"
         sizeZ="2.0*PassiveThick+ActiveThick" />
  </logvol>

  <logvol name="lvLModule" material="Air">
    <box name="LModBox"
         sizeX="LModXSize"
         sizeY="0.5*LModYSize"
         sizeZ="2.0*PassiveThick+ActiveThick" />
    <physvol name="pvLLeftSideWall"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvLSideWall">
      <posXYZ x="-0.5*(LModXSize-SideWallThick)" />
    </physvol>
    <physvol name="pvLModP1"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvLModuleP">
      <posXYZ z="-0.5*(ActiveThick+PassiveThick)" />
    </physvol>
    <physvol name="pvLModA"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvLModuleA">
    </physvol>
    <physvol name="pvLModP2"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvLModuleP">
      <posXYZ z="0.5*(ActiveThick+PassiveThick)" />
    </physvol>
    <physvol name="pvLRightSideWall"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvLSideWall">
      <posXYZ x="0.5*(LModXSize-SideWallThick)" />
    </physvol>
  </logvol>

  <!-- ************************ -->
  <!-- * Define the S1 module * -->
  <!-- ************************ -->

  <logvol name="lvS1ModuleA"
          material="OT/OTActiveMix"
          sensdet="GiGaSensDetTracker/OTSDet">
    <box name="S1ModABox"
         sizeX="S1ModXSize-2.0*SideWallThick"
         sizeY="S1ModYSize "
         sizeZ="ActiveThick" />
  </logvol>

  <logvol name="lvS1ModuleP" material="OT/OTPassiveMix">
    <box name="S1ModPBox"
         sizeX="S1ModXSize-2.0*SideWallThick"
         sizeY="S1ModYSize"
         sizeZ="PassiveThick" />
  </logvol>

  <logvol name="lvS1SideWall" material="OT/OTSideWallMix">
    <box name="S1SideWallBox"
         sizeX="SideWallThick"
         sizeY="S1ModYSize"
         sizeZ="2.0*PassiveThick+ActiveThick" />
  </logvol>

  <logvol name="lvS1Module" material="Air">
    <box name="S1ModBox"
         sizeX="S1ModXSize"
         sizeY="S1ModYSize"
         sizeZ="2.0*PassiveThick+ActiveThick" />
    <physvol name="pvS1LeftSideWall"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvS1SideWall">
      <posXYZ x="-0.5*(S1ModXSize-SideWallThick)" />
    </physvol>
    <physvol name="pvS1ModP1"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvS1ModuleP">
      <posXYZ z="-0.5*(ActiveThick+PassiveThick)" />
    </physvol>
    <physvol name="pvS1ModA"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvS1ModuleA">
      <posXYZ />
    </physvol>
    <physvol name="pvS1ModP2"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvS1ModuleP">
      <posXYZ z="0.5*(ActiveThick+PassiveThick)" />
    </physvol>
    <physvol name="pvS1RightSideWall"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvS1SideWall">
      <posXYZ x="0.5*(S1ModXSize-SideWallThick)" />
    </physvol>
  </logvol>

  <!-- ************************ -->
  <!-- * Define the S2 module * -->
  <!-- ************************ -->

  <logvol name="lvS2ModuleA"
          material="OT/OTActiveMix"
          sensdet="GiGaSensDetTracker/OTSDet">
    <box name="S2ModABox"
         sizeX="S2ModXSize-2.0*SideWallThick"
         sizeY="S2ModYSize"
         sizeZ="ActiveThick" />
  </logvol>

  <logvol name="lvS2ModuleP" material="OT/OTPassiveMix">
    <box name="S2ModPBox"
         sizeX="S2ModXSize-2.0*SideWallThick"
         sizeY="S2ModYSize"
         sizeZ="PassiveThick" />
  </logvol>

  <logvol name="lvS2SideWall" material="OT/OTSideWallMix">
    <box name="S2SideWallBox"
         sizeX="SideWallThick"
         sizeY="S2ModYSize"
         sizeZ="2.0*PassiveThick+ActiveThick" />
  </logvol>

  <logvol name="lvS2Module" material="Air">
    <box name="S2ModBox"
         sizeX="S2ModXSize"
         sizeY="S2ModYSize"
         sizeZ="2.0*PassiveThick+ActiveThick" />
    <physvol name="pvS2LeftSideWall"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvS2SideWall">
      <posXYZ x="-0.5*(S2ModXSize-SideWallThick)" />
    </physvol>
    <physvol name="pvS2ModP1"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvS2ModuleP">
      <posXYZ z="-0.5*(ActiveThick+PassiveThick)" />
    </physvol>
    <physvol name="pvS2ModA"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvS2ModuleA">
      <posXYZ />
    </physvol>
    <physvol name="pvS2ModP2"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvS2ModuleP">
      <posXYZ z="0.5*(ActiveThick+PassiveThick)" />
    </physvol>
    <physvol name="pvS2RightSideWall"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvS2SideWall">
      <posXYZ x="0.5*(S2ModXSize-SideWallThick)" />
    </physvol>
  </logvol>

  <!-- ************************* -->
  <!-- * Define the S3 module: * -->
  <!-- ************************* -->

  <logvol name="lvS3ModuleA"
          material="OT/OTActiveMix"
          sensdet="GiGaSensDetTracker/OTSDet">
    <box name="S3ModABox"
         sizeX="S3ModXSize-2.0*SideWallThick"
         sizeY="S3ModYSize"
         sizeZ="ActiveThick" />
  </logvol>

  <logvol name="lvS3ModuleP" material="OT/OTPassiveMix">
    <box name="S3ModPBox"
         sizeX="S3ModXSize-2.0*SideWallThick"
         sizeY="S3ModYSize"
         sizeZ="PassiveThick" />
  </logvol>

  <logvol name="lvS3SideWall" material="OT/OTSideWallMix">
    <box name="S3SideWallBox"
         sizeX="SideWallThick"
         sizeY="S3ModYSize"
         sizeZ="2.0*PassiveThick+ActiveThick" />
  </logvol>

  <logvol name="lvS3Module" material="Air">
    <box name="S3ModBox"
         sizeX="S3ModXSize"
         sizeY="S3ModYSize"
         sizeZ="2.0*PassiveThick+ActiveThick" />
    <physvol name="pvS3LeftSideWall"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvS3SideWall">
      <posXYZ x="-0.5*(S3ModXSize-SideWallThick)" />
    </physvol>
    <physvol name="pvS3ModP1"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvS3ModuleP">
      <posXYZ z="-0.5*(ActiveThick+PassiveThick)" />
    </physvol>
    <physvol name="pvS3ModA"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvS3ModuleA">
      <posXYZ />
    </physvol>
    <physvol name="pvS3ModP2"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvS3ModuleP">
      <posXYZ z="0.5*(ActiveThick+PassiveThick)" />
    </physvol>
    <physvol name="pvS3RightSideWall"
             logvol="/dd/Geometry/AfterMagnetRegion/T/OT/Modules/lvS3SideWall">
      <posXYZ x="0.5*(S3ModXSize-SideWallThick)" />
    </physvol>
  </logvol>

</DDDB>
